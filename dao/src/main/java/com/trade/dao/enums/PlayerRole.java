package com.trade.dao.enums;

public enum PlayerRole {
    USER,
    ADMIN,
    GUEST
}
