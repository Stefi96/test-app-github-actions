package com.trade.dao.repositories;

import com.trade.dao.models.Player;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.Optional;

@Repository
@Transactional(readOnly = true)
public interface PlayerRepository extends JpaRepository<Player, Long> {
    Optional<Player> findByEmail(String email);
    Optional<Player> findByUsername(String username);

    @Transactional
    @Modifying
    @Query("UPDATE Player p " +
            "SET p.enabled = TRUE WHERE p.email = ?1")
    int enablePlayer(String username);
}
