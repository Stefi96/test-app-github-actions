package com.trade.dao.repositories;

import com.trade.dao.models.FinishedGame;
import com.trade.dao.models.LeaderboardEntry;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;
@Repository
public interface FinishedGameRepository extends JpaRepository<FinishedGame, Long> {

    @Query("SELECT " +
            "    new com.trade.dao.models.LeaderboardEntry(fg.winnerId, COUNT(fg)) " +
            "FROM " +
            "    FinishedGame fg " +
            "GROUP BY " +
            "    fg.winnerId " +
            "HAVING " +
            "    fg.winnerId > 0 " +
            "ORDER BY " +
            "    COUNT(fg) desc")
    List<LeaderboardEntry> getLeaderboard();

    @Query("SELECT fg " +
            "FROM FinishedGame fg " +
            "WHERE fg.player1id = :id OR fg.player2id = :id")
    List<FinishedGame> getHistoryById(@Param(value = "id")Long id);
}
