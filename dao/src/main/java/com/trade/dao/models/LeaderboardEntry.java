package com.trade.dao.models;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@AllArgsConstructor
@ToString
public class LeaderboardEntry {

    private String username;
    private long userID;
    private long numberOfWins;
    public LeaderboardEntry(long userID, long numberOfWins) {
        this.userID = userID;
        this.numberOfWins = numberOfWins;
    }
}