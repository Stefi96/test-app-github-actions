package com.trade.dao.models;

import lombok.Data;

import java.util.Random;
import java.util.UUID;

@Data
public class Guest {
    public static final String GUEST_STRING = "guest_";
    private static final Random random = new Random();

    private Long id;
    private String name;

    public Guest() {
        id = Math.abs(random.nextLong());
        name = GUEST_STRING + id;
    }
}