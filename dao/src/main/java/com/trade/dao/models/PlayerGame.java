package com.trade.dao.models;

import com.trade.dao.models.abstracts.AbstractGame;
import lombok.Data;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Arrays;

@Data
public class PlayerGame extends AbstractGame {
    private static Integer idGenerator = 1;
    private Integer id;
    private Player player1;
    private Player player2;

    private int playerOneTime;

    private int playerTwoTime;

    public PlayerGame(Player player1, Player player2) {
        this.id = idGenerator++;

        this.player1 = player1;
        this.player2 = player2;

        this.result = new ArrayList<>(Arrays.asList(N,N,N,N,N,N,0,N,N,N,N,N,N,0));
        this.startedAt = LocalDateTime.now();
        this.opponentRefreshed = true;
        this.turn = true;
        this.playerOneTime = 5 * 60;
        this.playerTwoTime = 5 * 60;
    }
}