package com.trade.dao.models.abstracts;

import lombok.Data;
import lombok.extern.slf4j.Slf4j;
import java.time.LocalDateTime;
import java.util.List;

@Slf4j
@Data
public abstract class AbstractGame {
    protected static final Integer N = 4;
    protected Boolean turn;       //true - player1 | false - player2
    protected List<Integer> result;
    protected LocalDateTime startedAt;
    protected boolean opponentRefreshed;

    protected Integer sum(Integer... values){
        int boardSum = 0;

        for(Integer value : values)
            boardSum+=value;

        return boardSum;
    }

    protected Integer sumFirstPlayer(){
        return sum(result.get(0),result.get(1),result.get(2),result.get(3),result.get(4),result.get(5));
    }

    protected Integer sumSecondPlayer(){
        return sum(result.get(7),result.get(8),result.get(9),result.get(10),result.get(11),result.get(12));
    }

    public Boolean isOver(){
        return sumFirstPlayer()==0 || sumSecondPlayer()==0;
    }

    public void playMove(Integer moveIndex){

        if(result.get(moveIndex)<=0){
            return;
        }

        Integer stones = result.get(moveIndex);
        result.set(moveIndex,0);

        log.trace("Checks witch player's turn it is");
        if(Boolean.TRUE.equals(turn)){
            log.debug("It is player one turn and his stones is being moved");
            movePlayerOne(stones,moveIndex);
        }
        else {
            log.debug("It is player two turn and his stones is being moved");
            movePlayerTwo(stones,moveIndex);
        }

        if(Boolean.TRUE.equals(isOver())){
            log.debug("The game has ended!");
            endGame();
        }
    }

    protected void movePlayerOne(Integer stones, Integer moveIndex){
        while(stones > 0){
            Integer pointer = (++moveIndex)%14;

            if(pointer==13){
                continue;
            }
            else {
                result.set(pointer, result.get(pointer)+1);
            }
            stones--;
        }

        Integer pointer = moveIndex%14;

        if((pointer<6 && pointer>=0) && result.get(pointer) == 1  && result.get(12-pointer) != 0){
            result.set(6, result.get(6) + 1 + result.get(12-pointer));
            result.set(pointer,0);
            result.set(12-pointer,0);

            turn = !turn;
        }
        else if(moveIndex == 6){
            this.opponentRefreshed = false;
        }
        else {
            turn = !turn;
        }
    }

    protected void movePlayerTwo(Integer stones, Integer moveIndex){
        while(stones > 0){
            Integer pointer = (++moveIndex)%14;

            if(pointer==6){
                continue;
            }
            else {
                result.set(pointer, result.get(pointer)+1);
            }
            stones--;
        }

        Integer pointer = moveIndex%14;

        if((pointer>6 && pointer <=12) && result.get(pointer) == 1  && result.get(12-pointer) != 0){
            result.set(13, result.get(13) + result.get(pointer) + result.get(12-pointer));
            result.set(pointer,0);
            result.set(12-pointer,0);

            turn = !turn;
        }
        else if(moveIndex == 13){
            this.opponentRefreshed = false;
        }
        else {
            turn = !turn;
        }
    }

    protected void endGame(){
        result.set(6, result.get(6) + sumFirstPlayer());
        result.set(13, result.get(13) + sumSecondPlayer());
        this.setZeros();
    }

    protected void setZeros(){
        for(Integer i=0;i<6;i++){
            result.set(i,0);
        }

        for(Integer i=7;i<13;i++){
            result.set(i,0);
        }
    }


    @Override
    public String toString() {
        return result.toString();
    }
}
