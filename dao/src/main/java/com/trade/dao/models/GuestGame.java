package com.trade.dao.models;

import com.trade.dao.models.abstracts.AbstractGame;
import lombok.Data;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.UUID;

@Data
public class GuestGame extends AbstractGame {
    private static final String GAME_STRING = "game_";
    private String id;
    private Guest player1;
    private Guest player2;

    public GuestGame(Guest player1, Guest player2) {
        this.id = GAME_STRING + UUID.randomUUID();

        this.player1 = player1;
        this.player2 = player2;

        this.result = new ArrayList<>(Arrays.asList(N,N,N,N,N,N,0,N,N,N,N,N,N,0));
        this.startedAt = LocalDateTime.now();
        this.opponentRefreshed = true;
        this.turn = true;
    }
}
