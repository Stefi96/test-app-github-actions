package com.trade.dao.models;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.time.LocalDateTime;

@Entity
@Data
@AllArgsConstructor
@NoArgsConstructor
@Table
@Builder
public class FinishedGame {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;
    private String player1username;
    private long player1id;
    private String player2username;
    private long player2id;
    private long winnerId;
    private int player1score;
    private int player2score;
    private LocalDateTime startedAt;
    private LocalDateTime endedAt;
}
