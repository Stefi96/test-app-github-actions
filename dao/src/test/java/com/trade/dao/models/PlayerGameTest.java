package com.trade.dao.models;

import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

class PlayerGameTest {

    @Test
    void playMovePlayer1FinishesOnMancala() {
        PlayerGame playerGame = new PlayerGame(new Player(), new Player());

        List<Integer> finalResult = new ArrayList<>(Arrays.asList(4,4,0,5,5,5,1,4,4,4,4,4,4,0));

        playerGame.playMove(2);

        assertEquals(finalResult, playerGame.getResult());
        assertEquals(true, playerGame.getTurn());
    }

    @Test
    void playMovePlayer1FinishesOnHisPit(){
        PlayerGame playerGame = new PlayerGame(new Player(), new Player());

        List<Integer> finalResult = new ArrayList<>(Arrays.asList(4,0,5,5,5,5,0,4,4,4,4,4,4,0));

        playerGame.playMove(1);

        assertEquals(finalResult, playerGame.getResult());
        assertEquals(false, playerGame.getTurn());
    }


    @Test
    void playMovePlayer2FinishesOnMancala() {
        PlayerGame playerGame = new PlayerGame(new Player(), new Player());

        List<Integer> finalResult = new ArrayList<>(Arrays.asList(4,4,4,4,4,4,0,4,4,0,5,5,5,1));

        playerGame.setTurn(false);
        playerGame.playMove(9);

        assertEquals(finalResult, playerGame.getResult());
        assertEquals(false, playerGame.getTurn());
    }

    @Test
    void playMovePlayer2FinishesOnHisPit(){
        PlayerGame playerGame = new PlayerGame(new Player(), new Player());

        List<Integer> finalResult = new ArrayList<>(Arrays.asList(4,4,4,4,4,4,0,4,0,5,5,5,5,0));

        playerGame.setTurn(false);
        playerGame.playMove(8);

        assertEquals(finalResult, playerGame.getResult());
        assertEquals(true, playerGame.getTurn());
    }

    @Test
    void playMovePlayer1StealsStones(){
        PlayerGame playerGame = new PlayerGame(new Player(), new Player());

        List<Integer> finalResult = new ArrayList<>(Arrays.asList(0,5,5,5,0,4,5,4,0,4,4,4,4,0));

        playerGame.getResult().set(4,0);
        playerGame.playMove(0);

        assertEquals(finalResult, playerGame.getResult());
        assertEquals(false, playerGame.getTurn());
    }

    @Test
    void playMovePlayer2StealsStones(){
        PlayerGame playerGame = new PlayerGame(new Player(), new Player());

        List<Integer> finalResult = new ArrayList<>(Arrays.asList(4,0,4,4,4,4,0,0,5,5,5,0,4,5));

        playerGame.getResult().set(11,0);
        playerGame.setTurn(false);
        playerGame.playMove(7);

        assertEquals(finalResult, playerGame.getResult());
        assertEquals(true, playerGame.getTurn());
    }

    @Test
    void playMovePlayer1DoesntLeaveStonesEnemyMancala(){
        PlayerGame playerGame = new PlayerGame(new Player(), new Player());

        List<Integer> finalResult = new ArrayList<>(Arrays.asList(5,5,5,4,4,0,1,5,5,5,5,5,5,0));

        playerGame.getResult().set(5,10);
        playerGame.playMove(5);

        assertEquals(finalResult, playerGame.getResult());
        assertEquals(false, playerGame.getTurn());
    }

    @Test
    void playMovePlayer2DoesntLeaveStonesEnemyMancala(){
        PlayerGame playerGame = new PlayerGame(new Player(), new Player());

        List<Integer> finalResult = new ArrayList<>(Arrays.asList(5,5,5,5,5,5,0,5,5,5,4,4,0,1));

        playerGame.getResult().set(12,10);
        playerGame.setTurn(false);
        playerGame.playMove(12);

        assertEquals(finalResult, playerGame.getResult());
        assertEquals(true, playerGame.getTurn());
    }
}