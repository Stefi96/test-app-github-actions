import Navbar from "./Navbar";
import { useEffect, useState } from 'react';
import '../styles/History.scss'
import axios from "axios";
import CheckIcon from '@mui/icons-material/Check';
import CloseIcon from '@mui/icons-material/Close';
import { PLAYER_PATH } from "../Routes";

interface HistoryData {
    endedAt: number[],
    id: number, 
    player1id: number,
    player1score: number,
    player1username: string,
    player2id: number,
    player2score: number,
    player2username: string,
    startedAt: number[], 
    winnerId: number,
}

export default function History() {

    const [tableData, setTableData] = useState<HistoryData[]>([]);

    const [myID, setMyID] = useState<number>(0);

    useEffect(() => {
        axios.get(`${PLAYER_PATH}/history`, { withCredentials: true }).then((response) => {
            setTableData(response.data);
            axios.get(`${PLAYER_PATH}/my-id`, { withCredentials: true }).then((response) => {
                setMyID(response.data);
        }).catch((error) => { 
            console.log(error); 
        })
        }).catch((error) => {
            console.log(error);
        })
    }, [])

    const body = tableData.map((item => (
        <tr>
            <td>{ item.id }</td>
            <td>{ item.player1username }</td>
            <td>{ item.player2username }</td>
            <td>{ item.player1score }</td>
            <td>{ item.player2score }</td>
            <td>{ (item.startedAt[2] < 10 ? "0" + item.startedAt[2] : item.startedAt[2]) + "." + (item.startedAt[1] < 10 ? '0' + item.startedAt[1] : item.startedAt[1]) + "." + item.startedAt[0] + " - " +  (item.startedAt[3] < 10 ? '0' + item.startedAt[3] : item.startedAt[3]) + ":" + (item.startedAt[4] < 10 ? '0' + item.startedAt[4] : item.startedAt[4]) + ":" + (item.startedAt[5] < 10 ? '0' + item.startedAt[5] : item.startedAt[5])}</td>
            <td>{ (item.endedAt[2] < 10 ? "0" + item.endedAt[2] : item.endedAt[2]) + "." + (item.endedAt[1] < 10 ? '0' + item.endedAt[1] : item.endedAt[1]) + "." + item.endedAt[0] + " - " +  (item.endedAt[3] < 10 ? '0' + item.endedAt[3] : item.endedAt[3]) + ":" + (item.endedAt[4] < 10 ? '0' + item.endedAt[4] : item.endedAt[4]) + ":" + (item.endedAt[5] < 10 ? '0' + item.endedAt[5] : item.endedAt[5])}</td>
            <td className={myID === item.winnerId ? "winner-box" : "loser-box"}>{myID === item.winnerId ? <CheckIcon /> : <CloseIcon />}</td>
        </tr>
    )))

    return(

        <div className="history-background">
        <Navbar setSurrender={() => {}}/>
        
        <table className="history">
            <thead className="history-header">
                <tr>
                    <th>Game ID</th>
                    <th>Player 1</th>
                    <th>Player 2</th>
                    <th>Player 1 score</th>
                    <th>Player 2 score</th>
                    <th>Game started at</th>
                    <th>Game finnished at</th>
                    <th>Won/Lost</th>
                </tr>
            </thead>
            <tbody className="history-table">
                {body}
            </tbody>
        </table>
        </div>
    )
    
}