import "../styles/Username.scss";


interface Props {
    playerUsername: string
}

export default function Username({playerUsername} : Props) {


    return(
        <>
            <div className="username-background">
                <h1 className="username">{playerUsername}</h1>
            </div>
        </>
    )

}

