import "../styles/Loading.scss";

export default function Loading() {

    return(
        <>
            <div>
                <div className="loading-text">
                    <span className="loading-text-words font-size">FINDING</span>
                    <span className="loading-text-words"></span>
                    <span className="loading-text-words font-size">OPONENT</span>
                </div>
            </div>
        </>
    )

}