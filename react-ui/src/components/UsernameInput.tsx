import React from "react";
import { TextField, InputLabel, InputAdornment } from "@mui/material"
import AccountCircle from '@mui/icons-material/AccountCircle'
import "../styles/UsernameInput.scss"

interface Props {
    inputID: string,
    inputText: string,
    type: string,
    getValue: any,
    error: boolean,
    errorHelper: string,
}

export default function UsernameInput({inputID, inputText, type, getValue, error, errorHelper} : Props) {

    return(
        <>
            <br />
            <InputLabel htmlFor={inputID}>{inputText}</InputLabel>
            <TextField variant="standard" helperText={error ? errorHelper : ""} error={error} id={inputID} type={type} onChange={getValue} InputProps={{startAdornment: (
                <InputAdornment position="start">
                    <AccountCircle></AccountCircle>
                </InputAdornment>
            )}} />
            <br />
        </>
    );

}