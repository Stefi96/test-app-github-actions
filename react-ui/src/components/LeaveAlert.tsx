import '../styles/LeaveAlert.scss';
import { MouseEvent } from 'react';
import { Box, Button, Grid, Paper } from '@mui/material';

interface Props {
    onClickFunction: (event: MouseEvent<HTMLButtonElement>) => void,
}

export default function LeaveAlert({ onClickFunction } : Props) {

    return(
        <>
            <div className="overlay">
                <br /><br /><br /><br /><br />
                <Box sx={{ flexGrow: 2, mx: 'auto' }} className="box-margins">
                    <Grid container columns={12}>
                        <Grid item={true} xs={1} xl={4}>
                        </Grid>
                        <Grid item={true}  xs={10} xl={4}>
                            <Paper className="card-color">
                                <br />
                                <h1>If you leave, you will lose game!</h1>
                                <Button color="success" variant="contained" className="leave-button" onClick={onClickFunction}>Stay</Button>
                                <Button color="error" variant="contained" className="leave-button" onClick={onClickFunction}>Leave</Button>
                            </Paper>
                        </Grid>
                    </Grid>
                </Box>
            </div>
        </>
    )

}