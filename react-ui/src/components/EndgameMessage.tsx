import { Box, Grid, Card, CardContent, Typography, Button } from '@mui/material';
import { useState } from 'react';
import "../styles/Game.scss";
import axios from "axios";
import { GAME_PATH } from '../Routes';

interface Props {
    message: string,
}

export default function EndgameMessage({ message } : Props) {

    const [gameID, setGameID] = useState<string>(window.location.href.split('?')[1].split("=")[1]);

    const deleteGame = () => {
        axios.delete(`${GAME_PATH}/id=${gameID}`, { withCredentials: true }).then((response) => {
            console.log(response);
        }).catch((error) => { console.log(error); });
    }

    const newGameClick = () => {
        deleteGame();
        window.location.replace('/find-game');
    }

    const homepageClick = () => {
        deleteGame();
        window.location.replace('/');
    }

    return(
        <>
            <Box sx={{ flexGrow: 2, mx: 'auto' }} className="box-margins">
                <Grid container columns={12}>
                    <Grid item={true} xs={1} xl={4}>
                    </Grid>
                    <Grid item={true}  xs={10} xl={4}>        
                        <Card className="card-background" sx={{ minWidth: 275 }}>
                            <CardContent>
                                <Typography variant="h3" component="div" className="font">Mancala Game</Typography>
                                <br /><br />
                                <Typography variant="h5" className="font">{ message }</Typography>
                                <br /><br />
                                <div>
                                    <Button className='card-button' variant='contained' color='success' onClick={newGameClick}>New game</Button>
                                    <Button className='card-button' variant='contained' onClick={homepageClick}>Homepage</Button>
                                </div>
                            </CardContent> 
                        </Card>
                    </Grid> 
                </Grid>
            </Box>
        </>
    )

}