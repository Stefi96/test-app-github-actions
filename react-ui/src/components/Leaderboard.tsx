import axios from 'axios';
import { useEffect, useState } from 'react';
import { GAME_PATH } from '../Routes';
import '../styles/Leaderboard.scss';
import Navbar from './Navbar';

  interface LeaderBoardData {
    userID: number,
    username: string,
    numberOfWins: number
  }


export default function Leaderboard() {

  const [tableData, setTableData] = useState<LeaderBoardData[]>([]);

  useEffect(() => {
    axios.get(`${GAME_PATH}/leaderboard`, {withCredentials: true}).then((response) => {
      setTableData(response.data);
      console.log(response.data);
    }).catch((error) => {
      console.log(error);
    });
  }, []);

  let rank = 1;

  const body = tableData.map((item => (
    <tr>
      <td>{ rank++ }</td>
      <td>{ item.username }</td>
      <td>{ item.numberOfWins }</td>
    </tr>
  )))

  return (
    
    <div className='leaderboard-background'>
    <Navbar setSurrender={() => {}} />
    <table className='leaderboard'>
      <thead className='leaderboard-header'>
        <tr>
          <th>ID</th>
          <th>Username</th>
          <th>Number of Wins</th>
        </tr>
      </thead>
      <tbody className='leaderboard-table'>
        {body}
      </tbody>
    </table>
    </div>
  );
}