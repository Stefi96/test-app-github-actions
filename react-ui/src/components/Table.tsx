import { useState, useEffect} from 'react';
import Hole from "./Hole";
import Mancala from "./Mancala";
import "../styles/Table.scss"
import axios from 'axios';
import { GAME_PATH, PLAYER_PATH } from '../Routes';
import PlayerCard from './PlayerCard';

interface Props {
    playerTurn: boolean,
    player: number,
    result: number[], 
    getHolesArray: () => void,
}

export default function Table({ playerTurn, player, result, getHolesArray } : Props) {

    const [gameID, setGameID] = useState<string>(window.location.href.split('?')[1].split("=")[1]);

    const [playerOne, setPlayerOne] = useState<string>("");

    const [playerTwo, setPlayerTwo] = useState<string>("");

    const playMove = (id: number): void => {
        console.log(id);
        axios.get(`${GAME_PATH}/game=${gameID}&field=${id}`, { withCredentials: true }).then((response) => {
            console.log(response);
            if(response.data.result.length > 0) {
                getHolesArray();
            }
        }).catch((error) => { console.log(error); })
    }

    useEffect(() => {
        axios.get(`${PLAYER_PATH}/my-id`, { withCredentials: true }).then((response) => {
            let myID = response.data;
            let currentGameID = window.location.href.split('?')[1].split("=")[1];
            axios.get(`${GAME_PATH}/id=${currentGameID}`, { withCredentials : true }).then((response) => {
                if (response.data.player1.id === myID) {
                    setPlayerOne(response.data.player1.username);
                    setPlayerTwo(response.data.player2.username);
                } else if(response.data.player2.id === myID) {
                    setPlayerOne(response.data.player2.username);
                    setPlayerTwo(response.data.player1.username);
                }
            })
        }).catch((error) => { console.log(error); })
    }, [])

    return(
        <>
            <PlayerCard playerUsername={playerTwo} onTurn={playerTurn} />
            <table className="mancala-table">
                <tr>
                    <Mancala stoneNumber={player === 1 ? result[6] : result[13]} />
                    <Hole stoneNumber={player === 1 ? result[5] : result[12]} onMove={false} value={0} waiting={false} clickFunction={() => {}} />
                    <Hole stoneNumber={player === 1 ? result[4] : result[11]} onMove={false} value={0} waiting={false} clickFunction={() => {}} />
                    <Hole stoneNumber={player === 1 ? result[3] : result[10]} onMove={false} value={0} waiting={false} clickFunction={() => {}} />
                    <Hole stoneNumber={player === 1 ? result[2] : result[9]} onMove={false} value={0} waiting={false} clickFunction={() => {}} />
                    <Hole stoneNumber={player === 1 ? result[1] : result[8]} onMove={false} value={0} waiting={false} clickFunction={() => {}}/>
                    <Hole stoneNumber={player === 1 ? result[0] : result[7]} onMove={false} value={0} waiting={false} clickFunction={() => {}} />
                    <Mancala stoneNumber={player === 1 ? result[13] : result[6]} />
                </tr>
                <tr>
                    <Hole stoneNumber={player === 1 ? result[7] : result[0]} onMove={!playerTurn} value={player === 1 ? 7 : 0} waiting={playerTurn} clickFunction={playMove} />
                    <Hole stoneNumber={player === 1 ? result[8] : result[1]} onMove={!playerTurn} value={player === 1 ? 8 : 1} waiting={playerTurn} clickFunction={playMove} />
                    <Hole stoneNumber={player === 1 ? result[9] : result[2]} onMove={!playerTurn} value={player === 1 ? 9 : 2} waiting={playerTurn} clickFunction={playMove} />
                    <Hole stoneNumber={player === 1 ? result[10] : result[3]} onMove={!playerTurn} value={player === 1 ? 10 : 3} waiting={playerTurn} clickFunction={playMove} />
                    <Hole stoneNumber={player === 1 ? result[11] : result[4]} onMove={!playerTurn} value={player === 1 ? 11 : 4} waiting={playerTurn} clickFunction={playMove} />
                    <Hole stoneNumber={player === 1 ? result[12] : result[5]} onMove={!playerTurn} value={player === 1 ? 12 : 5} waiting={playerTurn} clickFunction={playMove} />
                </tr>
            </table>
            <PlayerCard playerUsername={playerOne} onTurn={!playerTurn} />
        </>
    )

}