import { useState, useEffect, MouseEvent } from 'react';
import AppBar from '@mui/material/AppBar';
import Box from '@mui/material/Box';
import Toolbar from '@mui/material/Toolbar';
import Button from '@mui/material/Button';
import axios from 'axios';
import "../styles/Navbar.scss";
import { PLAYER_PATH, GAME_PATH } from '../Routes';
import LeaveAlert from './LeaveAlert';
import Username from './Username';

interface Props {
    setSurrender: () => void,
}

export default function Navbar({ setSurrender } : Props) {

    const [logged, setLogged] = useState<boolean>(false);

    const [isHomepage, setIsHomepage] = useState<boolean>(window.location.href.split('/')[window.location.href.split('/').length - 1] === '' || window.location.href.split('/')[window.location.href.split('/').length - 1] === 'home');

    const [isGame, setIsGame] = useState<boolean>(window.location.href.split('/')[window.location.href.split('/').length - 1].startsWith("game"));

    const [isFind, setIsFind] = useState<boolean>(window.location.href.split('/')[window.location.href.split('/').length - 1].startsWith("find-game"));

    const [isLeaderboard, setIsLeaderboard] = useState<boolean>(window.location.href.split('/')[window.location.href.split('/').length - 1].startsWith("leaderboard"));

    const [isHistory, setIsHistory] = useState<boolean>(window.location.href.split('/')[window.location.href.split('/').length - 1].startsWith("history"));

    const [navClass, setNavClass] = useState<string>("");

    const [showAlert, setShowAlert] = useState<boolean>(false);

    const [redirectLink, setRedirectLink] = useState<string>("");

    const [player, setPlayer] = useState<string>("");

    useEffect(() => {
        axios.get(`${PLAYER_PATH}/logged`, { withCredentials: true }).then((response) => {
            console.log(response.data);
            setLogged(response.data);
            if (isFind) {
                setNavClass("header-find");
            } if (isHomepage) {
                setNavClass("header-homepage");
            } if (isGame) {
                setNavClass("header-game");
            } if (isLeaderboard) {
                setNavClass("header-leaderboard");
            } if (isHistory) {
                setNavClass("header-history")
            }
        }).catch((error) => { console.log(error); });
    }, []);

    const preventRedirect = (event: MouseEvent<HTMLElement>) => {
        const target = event.target as HTMLElement;
        const innerText = target.innerText;
        if (innerText === "STAY") {
            console.log(innerText);
            setShowAlert(false);
        }
        if (innerText === "LEAVE") {
            surrenderClick();
            if (redirectLink === "/logout") {
                logoutFunction();
                window.location.replace('/');
            } else {
                window.location.replace(redirectLink);
            }
        }
    }

    const loginClick = () => {
        if(isGame) {
            setShowAlert(true);
            setRedirectLink('/login')
        } else {
            window.location.replace("/login");
        }
    }

    const homeClick = () => {
        if(isGame) {
            setShowAlert(true);
            setRedirectLink('/');
        } else {
            window.location.replace("/");
        }
    }

    const leaderboardClick = () => {
        if(isGame) {
            setShowAlert(true);
            setRedirectLink('/leaderboard');
        } else {
            window.location.replace("/leaderboard");
        }
    }

    const historyClick = () => {
        if(isGame) {
            setShowAlert(true);
            setRedirectLink('/history');
        } else {
            window.location.replace("/history");
        }
    }

    const logoutFunction = () => { 
        axios.get(`${PLAYER_PATH}/logout`, { withCredentials: true }).then((response) => {
            if(response.data === "You are successfully logged out") {
                window.location.replace("/");
            }
        }).catch((error) => {
            console.log(error);
        });
    }

    const logoutClick = () => {
        if(isGame) {
            setShowAlert(true);
            setRedirectLink('/logout');
        } else {
            logoutFunction();
            window.location.replace("/");
        }
    }

    const playGame = () => {
        window.location.replace("/find-game");
    }

    useEffect(() => {
        axios.get(`${PLAYER_PATH}/me`, { withCredentials: true }).then((response) => {
            setPlayer(response.data.username)
        }).then((error) => {
            console.log(error);
        })
    }, [])

    const surrenderClick = () => {
        let currentGameID = window.location.href.split('?')[1].split("=")[1];
        axios.get(`${GAME_PATH}/end/id=${currentGameID}`, { withCredentials: true }).then((response) => {
            console.log(response);
            setSurrender();
        }).catch((error) => {
            console.log(error);
        })
    }

    return (
      <>
        { showAlert ? <LeaveAlert onClickFunction={preventRedirect} /> : ""}
        <Box sx={{ flexGrow: 1 }}>
          <AppBar position="static" className={ navClass + " header" }>
            
            <Toolbar>
            <Username playerUsername={player}/>
              { logged && !isGame && !isFind ? <Button color='inherit' onClick={ playGame }>Play Game</Button>: ""}
              { !isHomepage && !isGame ? <Button color='inherit' onClick={ homeClick }>Home</Button> : "" }
              { !isGame && !isLeaderboard ? <Button color='inherit' onClick={ leaderboardClick }>Leaderboard</Button> : "" }
              { logged && !isGame && !isHistory? <Button color='inherit' onClick={ historyClick }>History</Button> : ""}
              { isGame ? <Button color='inherit' onClick={ surrenderClick }>Surrender</Button> : ""}
              { logged ? <Button color='inherit' onClick={ logoutClick }>Logout</Button> : <Button color="inherit" onClick={ loginClick }>Login</Button> }
            </Toolbar>
          </AppBar>
        </Box>
      </>
    );


}