import { Box, Grid, Card, CardContent } from '@mui/material';
import Typography from "@mui/material/Typography";
import "../styles/Game.scss";

export default function NotFound() {

    return(
        <>
            <div className="background blur" />
            <Box sx={{ flexGrow: 2, mx: 'auto' }} className="box-margins">
                <Grid container columns={12}>
                    <Grid item={true} xs={1} xl={4}>
                    </Grid>
                    <Grid item={true}  xs={10} xl={4}>
                        <Card className="card-background" sx={{ minWidth: 275 }}>
                            <CardContent>
                                <Typography variant="h3" component="div" className="font">Mancala Game</Typography>
                                <br /><br />
                                <Typography variant="h5" className="font">Sorry, page not found!</Typography>
                                <br /><br />
                            </CardContent>
                        </Card>
                    </Grid>
                </Grid>
            </Box>
        </>
    );

}