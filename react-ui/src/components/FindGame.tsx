import { Box, Button, Card, CardContent, Grid } from "@mui/material";
import { useState, useEffect } from "react";
import Typography from "@mui/material/Typography";
import "../styles/Game.scss";
import axios from "axios";
import Loading from "./Loading";
import Navbar from "./Navbar";
import { PLAYER_PATH, GAME_PATH } from "../Routes";


export default function FindGame() {

    axios.get(`${PLAYER_PATH}/logged`, { withCredentials: true }).then((response) => {
        if(!response.data) {
            window.location.replace("/")
        }
    }).catch((error) => { console.log(error); });

    const [findingOponent, setFindingOponent] = useState<boolean>(false);

    const toQueue = () => {

        axios.get(`${GAME_PATH}/queue`, { withCredentials: true }).then((response) => {
            if(response.data === "Player added to queue!") {
                setFindingOponent(true);
                setInterval(() => {
                    axios.get(`${GAME_PATH}/create-game`, { withCredentials: true }).then((response) => {
                        if(response.data !== "") {
                            window.location.replace(`game?id=${response.data.id}`);
                        }
                    }).catch((error) => {console.log(error)})
                }, 1000);
            }
        }).catch((error) => {
            console.log(error);
        })
    }

    return(
        <>
            <div className="background blur" />
            <Navbar setSurrender={() => {}} />
            <Box sx={{ flexGrow: 2, mx: 'auto' }} className="box-margins">
                <Grid container columns={12}>
                    <Grid item={true} xs={1} xl={4}>
                    </Grid>
                    <Grid item={true}  xs={10} xl={4}>        
                        <Card className="card-background" sx={{ minWidth: 275 }}>
                                { !findingOponent ? 
                                <CardContent>
                                    <Typography variant="h3" component="div" className="font">Mancala Game</Typography>
                                    <br /><br />
                                    <Typography variant="h5" className="font">Welcome to Mancala game. If you want to play new game click on "Find game" button.</Typography>
                                    <br /><br />
                                    <Button variant="contained" className="font button-size" onClick={toQueue}>Find game</Button>
                                    <br />
                                </CardContent> :
                                <Loading />
                                }
                        </Card>
                    </Grid>
                </Grid>
            </Box>
        </>
    );

}