import { Button } from "@mui/material";
import axios from "axios";
import { useState, useEffect } from "react";
import { PLAYER_PATH } from "../Routes";
import "../styles/HomePage.scss";
import Navbar from "./Navbar";

export default function HomePage() {
    const [logged, setLogged] = useState<boolean>(false);

    useEffect(() => {
        axios
            .get(`${PLAYER_PATH}/logged`, { withCredentials: true })
            .then((response) => {
                setLogged(response.data);
            })
            .catch((error) => {
                console.log(error);
            });
    }, []);

    const playGameClick = () => {
        if (logged) {
            window.location.replace("/find-game");
        } else {
            window.location.replace("/login")
        }
    }

    return (
        <>
            <div className="bg" />
            <Navbar setSurrender={() => {}} />
            <div className="wrapper">
                <div className="box">
                    <h1 className="text">Welcome to Mancala</h1>
                    <div className="gamerules">
                        <a>
                            The Mancala 'board' is made up of two rows of six holes, or pits,
                            each. Next, four pieces -- marbles or stones -- are placed in each
                            of the 12 holes. Each player has a 'store' to the right side of
                            the Mancala board.
                            <br />
                            <br />
                            Play:
                            <br />
                            The game begins with one player picking up all of the pieces in
                            any one of the holes on his side. Moving counter-clockwise, the
                            player deposits one of the stones in each hole until the stones
                            run out.
                            <br />
                            <br />
                            1. If you run into your own store, deposit one piece in it. If you
                            run into your opponent's store, skip it.
                            <br />
                            2. If the last piece you drop is in your own store, you get a free
                            turn.
                            <br />
                            3. If the last piece you drop is in an empty hole on your side,
                            you capture that piece and any pieces in the hole directly
                            opposite.
                            <br />
                            4. Always place all captured pieces in your store.
                            <br />
                            <br />
                            Winning the game:
                            <br />
                            The game ends when all six spaces on one side of the Mancala board
                            are empty. The player who still has pieces on his side of the
                            board when the game ends captures all of those pieces. Count all
                            the pieces in each store. The winner is the player with the most
                            pieces.
                            <br />
                            <br />
                            Tips:
                            <br />
                            Planning ahead is essential to victory in board games like
                            Mancala. Try to plan two or three moves into the future.
                        </a>
                    </div>
                    <br /><br />
                    <Button className="playgame-button" onClick={ playGameClick }>Play game</Button>
                </div>
            </div>
        </>
    );
}
