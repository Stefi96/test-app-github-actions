import '../styles/Hole.scss';

interface Props {
    stoneNumber: number, 
    onMove: boolean,
    waiting: boolean, 
    clickFunction: (val: number) => void,
    value: number,
}

export default function Hole({ stoneNumber, onMove, waiting, clickFunction, value } : Props) {

    return(
        <>
            <td className={`td hole ${onMove ? "on-move" : ""} ${waiting ? "waiting" : ""}`} onClick={() => {onMove && clickFunction(value)}}>{ stoneNumber }</td>
        </>
    )

}