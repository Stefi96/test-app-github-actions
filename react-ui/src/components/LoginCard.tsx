import { useState, ChangeEvent } from "react";
import '../styles/LoginCard.scss';
import { Grid, Paper, Box, Alert, Link } from '@mui/material'
import LoginIcon from "./LoginIcon";
import UsernameInput from "./UsernameInput";
import PasswordInput from "./PasswordInput";
import FormButton from "./FormButton";
import axios from "axios";
import { PLAYER_PATH } from "../Routes";

export default function LoginCard() {

    const [username, setUsername] = useState<string>("");

    const [password, setPassword] = useState<string>("");

    const [usernameError, setUsernameError] = useState<boolean>(false);

    const [passwordError, setPasswordError] = useState<boolean>(false);

    const [showMessage, setShowMessage] = useState<boolean>(false);

    const [message, setMessage] = useState<string>("");


    const getUsernameValue = (event: ChangeEvent<HTMLButtonElement>) => {
        setUsername((event.target as HTMLInputElement).value);
    }

    const getPasswordValue = (event: ChangeEvent<HTMLInputElement | HTMLTextAreaElement>) => {
        setPassword((event.target as HTMLInputElement).value);
    }

    const doLogin = (): void => {
        setUsernameError(username === '');
        setPasswordError(password === '');
        if(username !== '' && password !== '') {
            axios.post(`${PLAYER_PATH}/login`, 
            {
                "username" : username,
                "password" : password
            },
            {
                headers: {
                    'Content-Type' : 'application/json'
                }, 
                withCredentials: true,
            }
            ).then((response) => {
                console.log(response);
                window.location.replace('/');
            }).catch((error) => {
                setMessage(error.response.data.message);
                setShowMessage(true);
            });
        }
    }

    return(
        <>
            <Box sx={{ flexGrow: 2, mx: 'auto' }} className="box-margins">
                <Grid container columns={12}>
                    <Grid item={true} xs={1} xl={4}>
                    </Grid>
                    <Grid item={true}  xs={10} xl={4}>
                        <Paper className="card-color">
                            <br /><br />
                            <LoginIcon icon="key" />
                            <Box className="signin-text">Sign in</Box>
                            <UsernameInput inputID="username-login" inputText="Enter username" type="text" getValue={getUsernameValue} error={usernameError} errorHelper={""} />
                            <br /><br />
                            <PasswordInput passwordID="login-password" labelText="Enter password" getValue={getPasswordValue} error={passwordError} enterFunction={doLogin} />
                            <br />
                            <Link href="/register">You don't have account?</Link>
                            <br /><br /><br />
                            <FormButton text={"Login"} clickEvent={doLogin} />
                            { showMessage === true ? <Alert severity="error">{ message }</Alert> : "" }
                        </Paper>
                    </Grid>
                </Grid>
            </Box>
        </>
    );

}
