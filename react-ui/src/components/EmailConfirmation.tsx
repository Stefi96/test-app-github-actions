import axios from 'axios';
import { useState, useEffect } from 'react';
import { PLAYER_PATH } from '../Routes';
import { Box, Grid, Paper, Alert, Button } from '@mui/material';
import "../styles/EmailConfirmation.scss";

export default function EmailConfirmation() {

    const [showError, setShowError] = useState<boolean>(false);

    const [message, setMessage] = useState<string>("");

    useEffect(() => {
        let path = PLAYER_PATH + "/" + window.location.href.split('/')[window.location.href.split('/').length - 1];
        console.log(PLAYER_PATH);
        axios.get(path, { withCredentials: true }).then((response) => {
            console.log(response)
        }).catch((error) => { 
            setShowError(true);
            setMessage(error.response.data.message); 
        });
    }, []);

    const redirectHome = () => {
        window.location.replace("/");
    }

    const redirectLogin = () => {
        window.location.replace("/login");
    }

    return(
        <>
            <Box sx={{ flexGrow: 2, mx: 'auto' }} className="box-margins">
                <Grid container columns={12}>
                    <Grid item={true} xs={1} xl={4}>
                    </Grid>
                    <Grid item={true}  xs={10} xl={4}>
                        <Paper className="card-color">
                            { showError ? <Alert severity="error"><h4>{ message }</h4></Alert> : <Alert severity="success">{ message }</Alert> }
                            <Button className='confirm-page-button confirm-page-margin-top' variant="contained" onClick={redirectHome}>Homepage</Button>
                            <br />
                            <Button className='confirm-page-button' variant="contained" onClick={redirectLogin}>Login</Button>
                        </Paper>
                    </Grid>
                </Grid>
            </Box>
        </>
    )

}