import { useState, KeyboardEvent, ChangeEvent } from "react";
import { InputLabel, InputAdornment, IconButton, TextField } from '@mui/material'
import { VisibilityOff, Visibility } from "@mui/icons-material"

interface Props {
    passwordID: string,
    labelText: string,
    getValue: (event: ChangeEvent<HTMLInputElement | HTMLTextAreaElement>) => void,
    error: boolean,
    enterFunction: () => void,
}

export default function PasswordInput({ passwordID, labelText, getValue, error, enterFunction } : Props) {

    const [showPassword, setShowPassword] = useState<boolean>(false);

    const togglePasswordVisibilityTrue = () => {
        setShowPassword(true);
    }

    const togglePasswordVisibilityFalse = () => {
        setShowPassword(false)
    }

    const loginOnEnterButton = (e: KeyboardEvent) => {
        if(e.key === 'Enter') {
            enterFunction();
        }
    }

    return(
        <>
            <InputLabel htmlFor="password-input">{labelText}</InputLabel>
            <TextField 
                variant="standard"
                error={error}
                type={showPassword ? 'text' : 'password'}
                id={passwordID}
                onChange={getValue}
                onKeyDown={loginOnEnterButton}
                InputProps= {{endAdornment: (
                    <InputAdornment position="end">
                        <IconButton
                            onClick={togglePasswordVisibilityTrue}
                            onMouseDown={togglePasswordVisibilityFalse}
                        >
                            {showPassword ? <VisibilityOff /> : <Visibility />}
                        </IconButton>
                    </InputAdornment>
                ) }}
            />
        </>
    )

}  