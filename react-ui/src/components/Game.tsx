import "../styles/Game.scss"
import Table from "./Table"
import { useState, useEffect } from "react";
import axios from "axios";
import EndgameMessage from "./EndgameMessage";
import Navbar from './Navbar';
import { GAME_PATH, PLAYER_PATH } from "../Routes";

export default function Game() {

    const [myTurn, setMyTurn] = useState<boolean>(false);

    const [player, setPlayer] = useState<number>(0);

    const [result, setResult] = useState<Array<number>>([]);

    const [showMessage, setShowMessage] = useState<boolean>(false);

    const [message, setMessage] = useState<string>("");

    const [gameID, setGameID] = useState<string>(window.location.href.split('?')[1].split("=")[1]);

    const setSurrender = () => { setMessage("You lost!"); }
    
    const getHolesArray = () => {
        axios.get(`${PLAYER_PATH}/my-id`, { withCredentials: true }).then((response) => {
            let myID = response.data;
            let currentGameID = window.location.href.split('?')[1].split("=")[1];
            axios.get(`${GAME_PATH}/id=${currentGameID}`, { withCredentials : true }).then((response) => {
                const currentPlayer = myID === response.data.player1.id ? 1 : 2;
                setPlayer(currentPlayer);
                if((currentPlayer === 1 && response.data.turn) || (currentPlayer === 2 && !response.data.turn)) {
                    setMyTurn(true);
                }
                if((currentPlayer === 1 && !response.data.turn) || (currentPlayer === 2 && response.data.turn)) {
                    setMyTurn(false);
                }
                let tempResult = response.data.result;
                let zeroCount = tempResult.filter((x: number) => x === 0).length >= 12;
                if(zeroCount) {
                    if (tempResult[6] === 48 || tempResult[13] === 48) {
                        console.log(`6: ${tempResult[6]}, 13: ${tempResult[13]}`);
                        let winner = response.data.result[6] > response.data.result[13] ? 1 : 2;
                        setMessage(myID !== winner ? "You won!" : "You lost!");
                    } else {
                        let winner = response.data.result[6] > response.data.result[13] ? 1 : 2;
                        setMessage(myID !== winner ? "You lost!" : "You won!");
                    }
                    setShowMessage(true);
                } else {
                    setResult(response.data.result);
                }
            }).catch((error) => {
                console.log(error);
                setShowMessage(true);
            })
        }).catch((error) => {
            if(error.response.status === 401) {
                window.location.replace("/")
            }
        })
    }

    useEffect(() => { 
        setInterval(getHolesArray, 1000); 
    }, []);

    return(
        <>
            <div className="background">
                { showMessage ? "" : <Navbar setSurrender={setSurrender} /> }
                { showMessage ? <EndgameMessage message={message} /> : <Table playerTurn={myTurn} player={player} result={result} getHolesArray={getHolesArray} /> }
            </div>
        </>
    )

}