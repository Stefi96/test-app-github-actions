import "../styles/PlayerCard.scss";
import { useState, useEffect, useCallback } from 'react';
import axios from "axios";
import { GAME_PATH } from "../Routes";

interface Props {
    playerUsername: string,
    onTurn: boolean,
}

export default function PlayerCard({ playerUsername, onTurn } : Props) {

    const [gameID, setGameID] = useState<string>(window.location.href.split('?')[1].split("=")[1]);

    const [displayTime, setDisplayTime] = useState<string>('');

    const [time, setTime] = useState<number>(300);

    const createStringTime = () => {
        console.log(time);
        let minutes = ~~(time / 60);
        let seconds = time - minutes * 60;
        setDisplayTime(`${minutes}:${seconds < 10 ? "0" + seconds : seconds + ""}`);
    }

    const surrender = () => {
        axios.get(`${GAME_PATH}/end/id=${gameID}`, { withCredentials: true }).then((response) => {
            console.log(response);
        }).catch((error) => {
            console.log(error);
        })
    }

    useEffect(() => {
        const interval = setInterval(() => { 
            if (onTurn) {
                axios.get(`${GAME_PATH}/decrease-time/id=${gameID}`, { withCredentials: true }).then((response) => {
                    if(response.data !== 0) {
                        setTime(response.data);
                        createStringTime();
                    } else {
                        surrender();
                    }
                }).catch((error) => {
                    console.log(error);
                })
            }
         }, 1000);
        return () => clearInterval(interval);
    }, [createStringTime, setTime, time]);

    return(
        <>
            <div className="playercard-background">
                <h1 className="player-text">{ playerUsername }</h1>
                <h1>{ displayTime }</h1>
            </div>
        </>
    )

}