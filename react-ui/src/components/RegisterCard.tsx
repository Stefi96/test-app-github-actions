import { Box, Grid, Paper, Alert } from "@mui/material";
import PasswordInput from "./PasswordInput";
import UsernameInput from "./UsernameInput";
import FormButton from "./FormButton";
import LoginIcon from "./LoginIcon";
import React from "react";
import "../styles/LoginCard.scss";
import axios from "axios";
import { GAME_PATH, PLAYER_PATH } from "../Routes";

interface State {
    username: String,
    email: string,
    password: String,
    re: String,
    usernameError: boolean,
    usernameHelper: string,
    emailError: boolean,
    emailHelper: string,
    passwordError: boolean,
    reError: boolean,
    showMessage: boolean,
    message: string,
}
 
export default function RegisterCard() {

    const [values, setValues] = React.useState<State>({
        username: '',
        email: '',
        password: '', 
        re: '', 
        usernameError: false,
        usernameHelper: "",
        emailError: false,
        emailHelper: "",
        passwordError: false,
        reError: false,
        showMessage: false,
        message: ""
    });

    const registerUser = () => {
        let regex = new RegExp('[a-z0-9]+@[a-z]+\.[a-z]{2,3}');
        setValues({...values, 
            usernameError: values.username === '', 
            emailError: !regex.test(values.email), 
            passwordError: (values.password === '' || values.password !== values.re), 
            reError: (values.re === '' || values.password !== values.re)});
        if(values.email !== '' && values.password !== '' && values.username !== '' && values.re !== '' && values.re === values.password) {
            axios.post(`${PLAYER_PATH}/register`, 
            { 
                "username" : values.username, 
                "password" : values.password, 
                "email" : values.email 
            },
            {
                headers: {
                    'Content-Type' : 'application/json'
                }
            }
            ).then((response) => {
                window.location.replace("/login");
            }).catch((error) => {
                setValues({...values, showMessage: true, message: error.response.data.message});
            })
        }
    }

    const getValue = (event: any) => {
        const propID = event.target.id;
        if(propID === 'username-register') {
            setValues({...values, username: event.target.value});
        } 
        if(propID === 'email-register') {
            setValues({...values, email: event.target.value})
        }
    }

    const getPasswordValues = (event: any) => {
        const propID = event.target.id;
        const value = event.target.value;
        if(propID === 'register-password') 
            setValues({...values, password: value});
        if(propID === 're-password')
            setValues({...values, re: value});
    }

    return(
        <>
            <Box sx={{ flexGrow: 2, mx: 'auto' }} className="box-margins">
                <Grid container columns={12}>
                    <Grid item={true} xs={1} xl={4}>
                    </Grid>
                    <Grid item={true}  xs={10} xl={4}>
                        <Paper className="card-color">
                            <br />
                            <LoginIcon icon="reg" />
                            <Box className="signin-text">Register</Box>
                            <UsernameInput inputID="username-register" inputText="Enter username" type="text" getValue={getValue} error={values.usernameError} errorHelper={values.usernameHelper} />
                            <UsernameInput inputID="email-register" inputText="Enter email" type="email" getValue={getValue} error={values.emailError} errorHelper={values.emailHelper} />
                            <br />
                            <PasswordInput passwordID="register-password" labelText="Enter password" getValue={getPasswordValues} error={values.passwordError} enterFunction={() => {}} />
                            <br /><br />
                            <PasswordInput passwordID="re-password" labelText="Re-enter password" getValue={getPasswordValues} error={values.reError} enterFunction={() => {}} />
                            <br /><br />
                            <FormButton clickEvent={registerUser} text={"Register"} />
                            { values.showMessage === true ? <Alert severity="error">{values.message}</Alert> : "" }
                        </Paper>
                    </Grid>
                </Grid>
            </Box>
        </>
    )

}