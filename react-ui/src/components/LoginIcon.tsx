import "../styles/LoginIcon.scss";
import VpnKeyIcon from '@material-ui/icons/VpnKey';
import { HowToReg } from "@mui/icons-material";

interface Props {
    icon: String,
}

function LoginIcon({ icon } : Props) {

    return(
        <>
            <div id="circle" className="circle-margin">
                { icon === 'key' ? <VpnKeyIcon fontSize="large" id="icon" /> : <HowToReg fontSize="large" id="icon" />}
            </div>
        </>
    )

}

export default LoginIcon;
