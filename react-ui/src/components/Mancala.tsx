import "../styles/Hole.scss"

interface Props {
    stoneNumber: number,
}

export default function Mancala({ stoneNumber } : Props) {

    return(
        <>
            <td rowSpan={2} className="td mancala">{stoneNumber}</td>
        </>
    )

}