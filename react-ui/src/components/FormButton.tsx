import { Button } from "@mui/material";
import '../styles/FormButton.scss'

interface Props {
    clickEvent: () => void,
    text: string
}

export default function FormButton({ clickEvent, text } : Props) {

    return(
        <>
            <Button className="button" variant="contained" onClick={clickEvent}>{text}</Button>
        </>
    );

}