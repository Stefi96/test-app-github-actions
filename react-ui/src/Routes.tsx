let url = window.location.href.split("/")[2];

let BASE = "http://";

if (url.startsWith('localhost')) {
	BASE = BASE + url.split(":")[0] + ":8080";
} else {
	BASE = BASE + url + ":8080";
}

const GAME_PATH = BASE + "/game"; 
const PLAYER_PATH = BASE;

export { GAME_PATH, PLAYER_PATH }
