import './App.css';
import LoginCard from './components/LoginCard';
import RegisterCard from './components/RegisterCard';
import Game from './components/Game';
import { BrowserRouter as Router, Routes, Route } from 'react-router-dom';
import FindGame from './components/FindGame';
import HomePage from './components/HomePage';
import Leaderboard from './components/Leaderboard';
import History from './components/History';
import NotFound from './components/NotFound';
import EmailConfirmation from './components/EmailConfirmation';

function App() {
  return (
    <div className="App">
      <Router>
        <Routes>
          <Route path='/login' element={<LoginCard />} />
          <Route path='/register' element={<RegisterCard />} />
          <Route path='/game' element={<Game />} />
          <Route path='/find-game' element={<FindGame />} />
          <Route path='/' element={<HomePage />} />
          <Route path='/home' element={<HomePage />} />
          <Route path='/leaderboard' element={<Leaderboard />} />
          <Route path='/history' element={<History />} />
          <Route path="*" element={<NotFound />} />
          <Route path="/confirm" element={<EmailConfirmation />} />
        </Routes>
      </Router>
    </div>
  );
}

export default App;
