package com.trade.web;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

@SpringBootApplication(scanBasePackages = {"com.trade.*", "com.trade.dao"})
@EnableJpaRepositories(basePackages = {"com.trade.dao.repositories"})
@EntityScan("com.trade.dao.models")
public class WebApplication {

    public static void main(String[] args) {
        SpringApplication.run(WebApplication.class, args);

        Logger logger = LogManager.getRootLogger();
        logger.info("Configuration File Defined To Be :: " + System.getProperty("log4j2-spring.xml"));
    }
}