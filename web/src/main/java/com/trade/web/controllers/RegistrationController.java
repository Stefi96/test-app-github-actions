package com.trade.web.controllers;

import com.trade.common.dto.RegistrationRequest;
import com.trade.common.interfaces.RegistrationService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

@Slf4j
@Controller
public class RegistrationController {

    @Autowired
    private RegistrationService registrationService;

    @GetMapping({"/signup"})
    public String signup(Model model){
        model.addAttribute("registrationRequest", new RegistrationRequest());
        return "register";
    }

    @GetMapping("/hello")
    @ResponseBody
    public String radi(){
        return "hello world";
    }

    @PostMapping("/signup")
    public String register (@RequestBody RegistrationRequest registrationRequest, Model model){
        System.out.println(registrationRequest.toString());
        try{
            String output = registrationService.register(registrationRequest);
            log.info("User [{}] has successfully registered. {}", registrationRequest.getUsername(), output);
        }
        catch (IllegalStateException illegalStateException){
            log.warn("IllegalStateException - {}", illegalStateException.getMessage());

            model.addAttribute("illegalStateException", illegalStateException);
            return "register";
        }
        return "redirect:/login";
    }

    @ExceptionHandler(IllegalStateException.class)
    @ResponseStatus(value = HttpStatus.BAD_REQUEST)
    public @ResponseBody Error handleException(IllegalStateException illegalStateException){
        return new Error(illegalStateException.getMessage());
    }

    @GetMapping("/confirm")
    @ResponseBody
    public String confirm(@RequestParam("token") String token){
        return registrationService.confirmToken(token);
    }
}
