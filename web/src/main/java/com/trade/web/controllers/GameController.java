package com.trade.web.controllers;

import com.trade.common.helper.SessionHelper;
import com.trade.common.interfaces.GameService;
import com.trade.common.interfaces.RoleModelService;
import com.trade.dao.enums.PlayerRole;
import com.trade.dao.models.Player;
import com.trade.dao.models.PlayerGame;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.ResponseBody;
import java.util.List;
import com.trade.dao.models.LeaderboardEntry;

@Slf4j
@Controller
public class GameController {

    @Autowired
    private GameService gameService;
    @Autowired
    private RoleModelService roleModelService;

    @GetMapping({"","/","/home"})
    private String landingPage(){
        return "home";
    }

    @GetMapping("/index")
    private String indexPage(Model model) {
        roleModelService.addRoleModel(model, PlayerRole.USER);
        return "index";
    }

    @GetMapping("/find_game")
    public synchronized String findGame(Model model){
        gameService.findGame();
        log.info("Player has joined the queue and waiting to find another player");
        roleModelService.addRoleModel(model, PlayerRole.USER);

        return "in_queue";
    }

    @GetMapping("/stop_queue")
    public synchronized String stopQueue(){
        gameService.removeCurrentPlayerFromQueue();
        log.info("Queue has been stop and player's is in the game");

        return "redirect:/index";
    }

    @GetMapping("/checkIfGameIsReady")
    @ResponseBody
    public PlayerGame checkIfGameIsReady(){
        return gameService.getCurrentGame();
    }

    @GetMapping("/game")
    public String foundGame(Model model){
        PlayerGame playerGame = gameService.getCurrentGame();
        Player player = (Player) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        model.addAttribute("game", playerGame);
        model.addAttribute("playerID", player.getId());
        roleModelService.addRoleModel(model, PlayerRole.USER);

        return "game";
    }

    @GetMapping("/play_move/{index}")
    public String playMove(@PathVariable Integer index){
        gameService.playMove(index);

        return "redirect:/game";
    }

    @GetMapping("/refreshGame")
    public String refreshGame(){
        gameService.refreshGame();

        return "redirect:/game";
    }

    @GetMapping("/leaderboard")
    public String getLeaderboard(Model model){
        List<LeaderboardEntry> leaderboard = gameService.getLeaderboard();
        model.addAttribute("leaderboard", leaderboard);

        return "leaderboard";
    }
}