package com.trade.web.controllers;

import com.trade.common.interfaces.GameService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;

@Controller
public class PlayerController {

    @Autowired
    private GameService gameService;

    @GetMapping("/showHistory")
    public String showProfile(Model model){
        model.addAttribute("history", gameService.getHistoryOfCurrentUser());

        return "profile";
    }
}
