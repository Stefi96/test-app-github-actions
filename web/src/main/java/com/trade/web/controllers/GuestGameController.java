package com.trade.web.controllers;

import com.trade.common.helper.SessionHelper;
import com.trade.common.interfaces.GuestGameService;
import com.trade.common.interfaces.GuestService;
import com.trade.common.interfaces.PlayerService;
import com.trade.common.interfaces.RoleModelService;
import com.trade.dao.enums.PlayerRole;
import com.trade.dao.models.GuestGame;
import com.trade.dao.models.Player;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

@Slf4j
@RequiredArgsConstructor
@Controller
@RequestMapping("/guest")
public class GuestGameController {
    private final GuestGameService guestGameService;
    private final GuestService guestService;
    private final RoleModelService roleModelService;
    private final PlayerService playerService;

    @GetMapping({"/", ""})
    public String getGamePage(Model model){
        guestService.loginNewGuest();
        roleModelService.addRoleModel(model, PlayerRole.GUEST);
        return "index";
    }

    @GetMapping("/find_game")
    public synchronized String findGame(Model model){
        guestGameService.findGame();
        log.info("Guest has joined the queue and waiting to find another player");
        roleModelService.addRoleModel(model, PlayerRole.GUEST);

        return "in_queue";
    }

    @GetMapping("/stop_queue")
    public synchronized String stopQueue(){
        guestGameService.removeCurrentGuestFromQueue();
        log.info("Queue has been stopped.");

        return "redirect:/guest";
    }

    @GetMapping("/checkIfGameIsReady")
    @ResponseBody
    public GuestGame checkIfGameIsReady(){
        return guestGameService.getCurrentGuestGame();
    }

    @GetMapping("/game")
    public String foundGame(Model model){
        GuestGame guestGame = guestGameService.getCurrentGuestGame();
        model.addAttribute("game", guestGame);
        model.addAttribute("playerID", SessionHelper.sessionGetID());
        roleModelService.addRoleModel(model, PlayerRole.GUEST);

        return "game";
    }

    @GetMapping("/play_move/{index}")
    public String playMove(@PathVariable Integer index){
        guestGameService.playMove(index);

        return "redirect:/guest/game";
    }

    @GetMapping("/refreshGame")
    public String refreshGame(){
        guestGameService.refreshGame();

        return "redirect:/guest/game";
    }


}