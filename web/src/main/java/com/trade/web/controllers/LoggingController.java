package com.trade.web.controllers;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
//import org.slf4j.Logger;
//import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
//@Slf4j
//@Log4j2
public class LoggingController {

    //Logger logger = LoggerFactory.getLogger(LoggingController.class);
    Logger logger = LogManager.getLogger(LoggingController.class);

    @RequestMapping("/logs")
    public String index() {

        logger.trace("A Trace message");
        logger.debug("A Debug message");
        logger.info("A Info message");
        logger.warn("A Warn message");
        logger.error("A Error message");

        return "Howdy! Check out the Logs to see the output...";
    }

}
