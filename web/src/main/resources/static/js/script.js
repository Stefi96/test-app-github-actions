let turn;
let play1;
let play2;

function displayResult(data){
    let res = data.result;
    turn = data.turn;
    playAgainst(data);
    displayTurn();
    populatePits(res);
    colorPits(turn);

}

function colorPits(turn){
    if((turn == true && (currentPlayer === play1))){
        for(let i = 0; i<6; i++){
            $("#"+i).css("border", "solid 1px green");
        }
    }else if(turn == false && (currentPlayer === play1)) {
        for(let i = 0; i<6; i++){
            $("#"+i).css("border", "solid 1px red");
        }
    }else if(turn == true && (currentPlayer === play2)){
        for(let i = 7; i<13; i++){
            $("#"+i).css("border", "solid 1px red");
        }
    }else if(turn == false && (currentPlayer === play2)){
        for(let i = 7; i<13; i++){
            $("#"+i).css("border", "solid 1px green");
        }
    }
}

function displayTurn(){
    if(currentPlayer === play1 && turn){
        $("#turn").text("it is your turn")
    }else if(currentPlayer === play2 && !turn){
        $("#turn").text("it is your turn")
    }else{
        $("#turn").text("it opponents turn")
    }
}

function playAgainst(data){
    if(currentPlayer === play1){
        $("#oponentLogin").text(data.player2.name);
    }else{
        $("#oponentLogin").text(data.player1.name);
    }
}


function turnTableforSecondPlayer(){
    $("#6").attr("id","13");
    $("#13").attr("id","6");
    for(let i = 0; i<13; i++){
        if(i!=6){
            if(i<6){
                $("#"+i).attr("id",i+7);
            }else{
                $("#"+i).attr("id",i-7);
            }
        }
    }
}

function populatePits(res){
    for(let i= 0; i<res.length;i++){
        $("#"+i).text(res.at(i));
    }
}

$(".pit").click(function () {
    var slot = $(this).attr('id');
    if(slot === "6" || slot ==="13"){
        alert("You cant play Mancala pit");
    }else{
        if((turn == true && (currentPlayer === play2)) || (turn == false && (currentPlayer === play1))  ){
            alert("You cant play its opponents turn");
        }else{
            if((currentPlayer === play1) && (slot === "7" || slot ==="8" || slot === "9" ||
                                            slot ==="10" || slot === "11" || slot ==="12" )){
                alert("thats not your pit");

            }else if((currentPlayer === play2) && (slot === "0" || slot ==="1" || slot === "2" ||
                                                    slot ==="3" || slot === "4" || slot ==="5" )){
                alert("thats not your pit");
            }else{

                makeMove(slot)
            }
        }
    }

});

function makeMove(slot){
    $.ajax({
        url: url + "/game/gameplay",
        type: 'POST',
        dataType: "json",
        contentType: "application/json",
        data: JSON.stringify({
                "gameId": gameId,
                "index" : slot
        }),
        success: function (data){
            displayResult(data)
        },
        error: function (error){
            console.log("error");
        }
    })
}

function getGame(gameId){
    $.ajax({
        url: url + "/game/getgame",
        type: 'POST',
        dataType: "json",
        contentType: "application/json",
        data: JSON.stringify({
            "gameId": gameId
        }),
        success: function (response){
            play1 = response.player1.name;
            play2 = response.player2.name;
            turn = response.turn;
        },
        error: function (error){
            console.log("error");
        }
    })
}

function reset(){
    for(let i= 0; i<14;i++){
        if(i == 6 || i == 13){
            $("#"+i).text(0);
        }else{
            $("#"+i).text(4);
        }
    }
}


