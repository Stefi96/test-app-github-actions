package com.trade.web.controllers;

import com.trade.common.interfaces.GameService;
import com.trade.dao.repositories.ConfirmationTokenRepository;
import com.trade.dao.repositories.FinishedGameRepository;
import com.trade.dao.repositories.PlayerRepository;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.web.servlet.MockMvc;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

@WebMvcTest(PlayerController.class)
@AutoConfigureMockMvc(addFilters = false)
class PlayerControllerTest {

    @MockBean
    private GameService gameService;
    @MockBean
    private ConfirmationTokenRepository confirmationTokenRepository;
    @MockBean
    private PlayerRepository playerRepository;
    @MockBean
    private FinishedGameRepository finishedGameRepository;
    @Autowired
    private MockMvc mockMvc;

    @Test
    void showProfile() throws Exception {
        mockMvc.perform(get("/showHistory"))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.model().attributeExists("history"))
                .andExpect(MockMvcResultMatchers.view().name("profile"));
    }
}