package com.trade.web.controllers;

import com.trade.common.interfaces.RegistrationService;
import com.trade.dao.repositories.ConfirmationTokenRepository;
import com.trade.dao.repositories.FinishedGameRepository;
import com.trade.dao.repositories.PlayerRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.*;
import org.springframework.test.web.servlet.MockMvc;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;


import static org.junit.jupiter.api.Assertions.*;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@WebMvcTest(RegistrationController.class)
@AutoConfigureMockMvc(addFilters = false)
public class RegistrationControllerTest {

    @Autowired
    private MockMvc mvc;

    @InjectMocks
    private RegistrationController registrationController;

    @MockBean
    private ConfirmationTokenRepository confirmationTokenRepository;

    @MockBean
    private RegistrationService registrationService;

    @MockBean
    private PlayerRepository playerRepository;

    @MockBean
    private FinishedGameRepository finishedGameRepository;

    @BeforeEach
    public void setup() {

    }

    @Test
    public void radi() throws Exception {

        String bla = registrationController.radi();
        assertEquals("hello world", bla);

        //mvc.perform(get("/hello")).andExpect(status().isOk());

    }

    @Test
    public void register() throws Exception {

        mvc.perform(post("/signup")
                .contentType(MediaType.APPLICATION_JSON)
                .content("{\"username\": \"user10\", \"password\": \"user10\", \"email\": \"user10@gmail.com\"}"))
                .andDo(print())
                .andExpect(status().is3xxRedirection());

    }

    @Test
    public void confirm() throws Exception {

        String token = "7b8716ee-c064-4d44-b8d1-7b644e024474";

        mvc.perform(get("/confirm").param("token", token)).andDo(print()).andExpect(status().isOk());
    }
}