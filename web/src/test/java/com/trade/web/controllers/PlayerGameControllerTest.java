package com.trade.web.controllers;

import com.trade.common.interfaces.GameService;
import com.trade.common.interfaces.RoleModelService;
import com.trade.common.services.RoleModelServiceImpl;
import com.trade.dao.repositories.ConfirmationTokenRepository;
import com.trade.dao.repositories.FinishedGameRepository;
import com.trade.dao.repositories.PlayerRepository;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@WebMvcTest(GameController.class)
@AutoConfigureMockMvc(addFilters = false)
@RunWith(SpringRunner.class)
class PlayerGameControllerTest {

    @MockBean
    private GameService gameService;
    @MockBean
    private ConfirmationTokenRepository confirmationTokenRepository;
    @MockBean
    private PlayerRepository playerRepository;
    @MockBean
    private FinishedGameRepository finishedGameRepository;

    @MockBean
    private RoleModelService roleModelService;
    @Autowired
    private MockMvc mockMvc;

    @Test
    void findGame() throws Exception{
        mockMvc.perform(get("/find_game"))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.view().name("in_queue"));
    }

    @Test
    void stopQueue() throws Exception{
        mockMvc.perform(get("/stop_queue"))
                .andExpect(MockMvcResultMatchers.status().is(302))
                .andExpect(MockMvcResultMatchers.view().name("redirect:/index"));
    }

    @Test
    void checkIfGameIsReady() throws Exception{
        mockMvc.perform(get("/checkIfGameIsReady"))
                .andExpect(MockMvcResultMatchers.status().isOk());
    }

//    @Test
//    @WithMockUser(username = "user1", password = "user1")
//    void foundGame() throws Exception{
//        mockMvc.perform(get("/find_game"));
//        mockMvc.perform(get("/find_game"));
//                .andExpect(MockMvcResultMatchers.status().isOk());
//        Player player1 = mock(Player.class);
//        Player player2 = mock(Player.class);
//        Game game = mock(Game.class);
//
//        game.setPlayer1(player1);
//        game.setPlayer2(player2);
//
//        gameService.getCurrentGame().setPlayer1(player1);
//        gameService.getCurrentGame().setPlayer1(player2);
//        System.out.println("--------------------");
//        System.out.println(gameService.getCurrentPlayer());
//        System.out.println("--------------------");
//        gameService.createGame();
//
//        mockMvc.perform(get("/game"))
//                .andExpect(MockMvcResultMatchers.status().isOk())
//                .andExpect(MockMvcResultMatchers.model().attributeExists("game"))
//                .andExpect(MockMvcResultMatchers.view().name("game"));
//    }

    @Test
    void playMove() throws Exception{
        mockMvc.perform(get("/play_move/{index}", 1))
                .andExpect(status().is(302))
                .andExpect(MockMvcResultMatchers.view().name("redirect:/game"));
    }

    @Test
    void refreshGame() throws Exception {
        mockMvc.perform(get("/refreshGame"))
                .andExpect(MockMvcResultMatchers.status().is(302))
                .andExpect(MockMvcResultMatchers.view().name("redirect:/game"));
    }

    @Test
    void getLeaderboard() throws Exception {
        mockMvc.perform(get("/leaderboard"))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.view().name("leaderboard"))
                .andExpect(MockMvcResultMatchers.model().attributeExists("leaderboard"));
    }
}