const url = "http://localhost:8080";
let stopmClient;
let gameId;
let currentPlayer;

function connectToSocket(gameId){
    let socket = new SockJS(url+"/gameplay");
    stopmClient = Stomp.over(socket);
    stopmClient.connect({}, function (frame){
        console.log("connected to frame: " + frame);
        stopmClient.subscribe("/topic/game-progress/"+gameId, function (response){
            let data = JSON.parse(response.body);
            displayResult(data);
            console.log(data);
        })
    })
}

function disconnectFromSocket(gameId){


}

function createGame(){
    let username = document.getElementById("username").value;
    if(username == null || username === ''){
        alert("Please enter username")
    }else{
        $.ajax({
            url: url + "/game/start",
            type: 'POST',
            dataType: "json",
            contentType: "application/json",
            data: JSON.stringify({
                "name": username
            }),
            success: function (data){
                gameId = data.gameId;
                connectToSocket(gameId);
                alert("You created a game with id: " + data.gameId);
                console.log(data)
                $("#playerName").text(data.player1.name);

                getGame(gameId);
                currentPlayer = username;
                reset();
                $("#conToRandom").prop("disabled", true);
            },
            error: function (error){
                console.log("error");
            }
        })
    }
}

function createGameWithFriend(){
    let username = document.getElementById("username").value;
    if(username == null || username === ''){
        alert("Please enter username")
    }else{
        $.ajax({
            url: url + "/game/startWithFriend",
            type: 'POST',
            dataType: "json",
            contentType: "application/json",
            data: JSON.stringify({
                "name": username
            }),
            success: function (data){
                gameId = data.gameId;
                connectToSocket(gameId);
                alert("You created a game with id: " + data.gameId);
                console.log(data)
                $("#playerName").text(data.player1.name);

                getGame(gameId);
                currentPlayer = username;
                reset();
                $("#conToSpec").prop("disabled", true);
            },
            error: function (error){
                console.log("error");
            }
        })
    }

}

function connectToRandom(){
    let username = document.getElementById("username").value;
    if(username == null || username === '') {
        alert("Please enter username")
    }else{
        $.ajax({
            url: url+ "/game/connect/random",
            type: 'POST',
            dataType: "json",
            contentType: "application/json",
            data: JSON.stringify({
                "name": username
            }),
            success: function (data){
                gameId = data.gameId;
                connectToSocket(gameId);
                $("#playerName").text(data.player2.name);
                alert("you are playing against: " + data.player1.name);
                getGame(gameId);
                currentPlayer = username;
                turnTableforSecondPlayer();
                displayTurn();
                playAgainst(data);
                reset();

            },
            error: function (error){
                console.log(error);
                alert(" there is no game ");
            }
        })
    }
}

function connectToSpecificGame(){
    let username = document.getElementById("username").value;
    if(username == null || username === '') {
        alert("Please enter username")
    }else{
        let game_Id = document.getElementById("game_id").value;
        if(game_Id == null || game_Id === ''){
            alert("please enter game id");
        }
        $.ajax({
            url: url + "/game/connect",
            type: 'POST',
            dataType: "json",
            contentType: "application/json",
            data: JSON.stringify({
                "player": {"name": username},
                "gameId": game_Id
            }),
            success: function (data){
                gameId = data.gameId;
                connectToSocket(gameId);
                $("#playerName").text(data.player2.name);
                alert("you are playing against: " + data.player1.name);
                getGame(gameId);
                currentPlayer = username;
                turnTableforSecondPlayer();
                displayTurn();
                playAgainst(data);
                reset();
            },
            error: function (error) {
                console.log(error);
                alert(" there is no game ");
            }
        })
    }
}


