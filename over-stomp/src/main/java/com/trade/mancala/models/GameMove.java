package com.trade.mancala.models;


import lombok.Data;

@Data
public class GameMove {


    private String gameId;
    private Integer index;

}
