package com.trade.mancala.models;

public enum GameStatus {
    NEW, IN_PROGRESS, FINISHED
}
