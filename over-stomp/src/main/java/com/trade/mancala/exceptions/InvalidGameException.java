package com.trade.mancala.exceptions;

public class InvalidGameException extends Throwable {
    private String message;

    public InvalidGameException(String message) {
        this.message = message;
    }

    public String getMessage() {
        return message;
    }
}
