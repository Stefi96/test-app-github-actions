package com.trade.mancala.exceptions;

public class InvalidParamException extends Throwable {
    private String message;

    public InvalidParamException(String message) {
        this.message = message;
    }

    public String getMessage() {
        return message;
    }
}
