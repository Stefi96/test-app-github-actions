package com.trade.mancala.controllers;


import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller()
public class WindowController {

    @RequestMapping("/playRandom")
    public String getGamePage(){
        return "redirect:/guestGame.html";
    }

    @RequestMapping("/playWithFriend")
    public String getGameWithFriendPage(){
        return "redirect:/guestGameWithFriend.html";
    }


}
