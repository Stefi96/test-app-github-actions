package com.trade.mancala.controllers.dto;

import lombok.Data;

@Data
public class GameID {
    private String gameId;
}
