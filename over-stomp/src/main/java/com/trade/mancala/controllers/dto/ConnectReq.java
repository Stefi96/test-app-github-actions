package com.trade.mancala.controllers.dto;

import com.trade.mancala.models.Player;
import lombok.Data;

@Data
public class ConnectReq {
    private Player player;
    private String gameId;
}
