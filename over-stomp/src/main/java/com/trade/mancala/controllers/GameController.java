package com.trade.mancala.controllers;


import com.trade.mancala.controllers.dto.ConnectReq;
import com.trade.mancala.controllers.dto.GameID;
import com.trade.mancala.exceptions.InvalidGameException;
import com.trade.mancala.exceptions.InvalidParamException;
import com.trade.mancala.exceptions.NotFoundException;
import com.trade.mancala.models.Game;
import com.trade.mancala.models.GameMove;
import com.trade.mancala.models.Player;
import com.trade.mancala.repo.GameStorage;
import com.trade.mancala.repo.GameStorageWithFriend;
import com.trade.mancala.services.GameService;
import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.messaging.simp.SimpMessagingTemplate;
import org.springframework.web.bind.annotation.*;

@RestController
@Slf4j
@AllArgsConstructor
@RequestMapping("/game")
public class GameController {
    @Autowired
    private final GameService gameService;
    @Autowired
    private final SimpMessagingTemplate simpMessagingTemplate;


    @PostMapping("/start")
    public ResponseEntity<Game> startGame(@RequestBody Player player){
        log.info("start game by : {}", player);
        return ResponseEntity.ok(gameService.createGame(player));
    }

    @PostMapping("/startWithFriend")
    public ResponseEntity<Game> startGameWithFriend(@RequestBody Player player){
        log.info("start game by : {}", player);
        return ResponseEntity.ok(gameService.createGameWithFriend(player));
    }

    @PostMapping("/connect/random")
    public ResponseEntity<Game> connectRandom(@RequestBody Player player2) throws NotFoundException {
        log.info("connect radnom player: {}", player2);
        Game game = gameService.connectToRandomGame(player2);
        simpMessagingTemplate.convertAndSend("/topic/game-progress/" + game.getGameId(), game);
        return ResponseEntity.ok(game);
    }

    @PostMapping("/connect")
    public ResponseEntity<Game> connect(@RequestBody ConnectReq req) throws InvalidParamException, InvalidGameException {
        log.info("connect req: {}", req);
        Game game = gameService.connectToGame(req.getPlayer(),req.getGameId());
        simpMessagingTemplate.convertAndSend("/topic/game-progress/" + game.getGameId(), game);
        return ResponseEntity.ok(game);
    }

    @PostMapping("/gameplay")
    public ResponseEntity<Game> gamePlay(@RequestBody GameMove gameMove){
        Game game = gameService.gamePlay(gameMove);
        simpMessagingTemplate.convertAndSend("/topic/game-progress/" + game.getGameId(), game);
        return ResponseEntity.ok(game);
    }

    @PostMapping("/getgame")
    public Game connect(@RequestBody GameID id) throws InvalidParamException, InvalidGameException {
        log.info("connect req: {}", id.getGameId());
        Game game = GameStorage.getInstance().getGames().get(id.getGameId());
        if(game == null){
            game = GameStorageWithFriend.getInstance().getGames().get(id.getGameId());
        }
        log.info(String.valueOf(game));
        return game;
    }






}
