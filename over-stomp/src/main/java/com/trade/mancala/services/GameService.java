package com.trade.mancala.services;


import com.trade.mancala.exceptions.InvalidGameException;
import com.trade.mancala.exceptions.InvalidParamException;
import com.trade.mancala.exceptions.NotFoundException;
import com.trade.mancala.models.Game;
import com.trade.mancala.models.GameMove;
import com.trade.mancala.models.GameStatus;
import com.trade.mancala.models.Player;
import com.trade.mancala.repo.GameStorage;
import com.trade.mancala.repo.GameStorageWithFriend;
import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;
import org.springframework.data.crossstore.ChangeSetPersister;
import org.springframework.stereotype.Service;

import java.security.InvalidParameterException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.UUID;

import static com.trade.mancala.models.GameStatus.IN_PROGRESS;
import static com.trade.mancala.models.GameStatus.NEW;

@Service
public class GameService {

    public Game createGame(Player player){
        Game game = new Game();
        game.setResult(new ArrayList<>(Arrays.asList(4,4,4,4,4,4,0,4,4,4,4,4,4,0)));
        game.setGameId(UUID.randomUUID().toString());
        game.setPlayer1(player);
        game.setTurn(true);
        game.setStatus(NEW);
        GameStorage.getInstance().setGames(game);
        return game;
    }

    public Game createGameWithFriend(Player player){
        Game game = new Game();
        game.setResult(new ArrayList<>(Arrays.asList(4,4,4,4,4,4,0,4,4,4,4,4,4,0)));
        game.setGameId(UUID.randomUUID().toString());
        game.setPlayer1(player);
        game.setTurn(true);
        game.setStatus(NEW);
        GameStorageWithFriend.getInstance().setGames(game);
        return game;
    }

    public Game connectToGame(Player player2, String gameId) throws InvalidParamException, InvalidGameException {
        if(!GameStorageWithFriend.getInstance().getGames().containsKey(gameId)){
            throw new InvalidParamException("Game withs this id dont exist");
        }
        Game game = GameStorageWithFriend.getInstance().getGames().get(gameId);

        if(game.getPlayer2() != null){
            throw new InvalidGameException("Game already in play");
        }
        game.setPlayer2(player2);
        game.setStatus(IN_PROGRESS);
        GameStorageWithFriend.getInstance().setGames(game);
        return game;
    }

    public Game connectToRandomGame(Player player2) throws NotFoundException {
        Game game = GameStorage.getInstance().getGames().values().stream()
                .filter(i -> i.getStatus().equals(NEW))
                .findFirst().orElseThrow(() -> new NotFoundException("Game not found"));
        game.setPlayer2(player2);
        game.setStatus(IN_PROGRESS);
        GameStorage.getInstance().setGames(game);
        return game;
    }


    public Game gamePlay(GameMove gameMove){
        Game game = GameStorage.getInstance().getGames().get(gameMove.getGameId());
        if(game == null){
            game = GameStorageWithFriend.getInstance().getGames().get(gameMove.getGameId());
            game.makeMove(gameMove.getIndex());
            GameStorageWithFriend.getInstance().setGames(game);
        }else{
            game.makeMove(gameMove.getIndex());
            GameStorage.getInstance().setGames(game);
        }
        return game;
    }
}
