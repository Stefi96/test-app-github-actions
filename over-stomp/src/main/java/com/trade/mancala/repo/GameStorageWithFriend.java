package com.trade.mancala.repo;

import com.trade.mancala.models.Game;

import java.util.HashMap;
import java.util.Map;

public class GameStorageWithFriend {
    private static Map<String, Game> games;
    private  static GameStorageWithFriend instance;

    private GameStorageWithFriend(){
        games = new HashMap<>();
    }

    public static synchronized GameStorageWithFriend getInstance(){
        if(instance == null){
            instance = new GameStorageWithFriend();
        }
        return instance;
    }

    public Map<String, Game> getGames(){
        return games;
    }
    public void setGames(Game game){
        games.put(game.getGameId(), game);
    }

}
