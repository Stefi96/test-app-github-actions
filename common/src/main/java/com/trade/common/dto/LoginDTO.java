package com.trade.common.dto;

import lombok.Data;

/**
 * @since 0.0.1
 * @version 1.0
 * @author Jovan Bojovic
 */
@Data
public class LoginDTO {

    private String username;
    private String password;

}
