package com.trade.common.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class GameTimeDTO {

    private long playerOneID;
    private int playerOneTime;
    private long playerTwoID;
    private int playerTwoTime;

}
