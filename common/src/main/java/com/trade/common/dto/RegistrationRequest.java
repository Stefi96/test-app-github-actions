package com.trade.common.dto;

import lombok.*;

/**
 * @since 0.0.1
 * @version 1.0
 * @author Jovan Bojovic, Dusan Markovic
 */
@Getter
@Setter
@AllArgsConstructor
@EqualsAndHashCode
@NoArgsConstructor
@ToString
public class RegistrationRequest {
    private String username;
    private String email;
    private String password;
}