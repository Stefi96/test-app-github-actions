package com.trade.common.dto;

import com.trade.dao.enums.PlayerRole;
import com.trade.dao.models.Player;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @since 0.0.1
 * @version 1.0
 * @author Jovan Bojovic
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class PlayerDataDTO {

    private String username;
    private String email;
    private PlayerRole role;

    public static class PlayerDataDTOBuilder {
        private String username;
        private String email;
        private PlayerRole role;

        public static PlayerDataDTOBuilder newInstance() {
            return new PlayerDataDTOBuilder();
        }

        public PlayerDataDTOBuilder setUsername(String username) {
            this.username = username;
            return this;
        }

        public PlayerDataDTOBuilder setEmail(String email) {
            this.email = email;
            return this;
        }

        public PlayerDataDTOBuilder setRole(PlayerRole role) {
            this.role = role;
            return this;
        }

        public PlayerDataDTO build() {
            return new PlayerDataDTO(username, email, role);
        }
    }

}
