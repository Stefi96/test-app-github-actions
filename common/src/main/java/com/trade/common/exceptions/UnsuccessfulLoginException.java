package com.trade.common.exceptions;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

/**
 * @version 1.0
 * @since 1.0.2
 * @author Jovan Bojovic
 */
@ResponseStatus(value = HttpStatus.UNAUTHORIZED, reason = "Invalid username or password!")
public class UnsuccessfulLoginException extends RuntimeException {

    public UnsuccessfulLoginException() {
        super();
    }

}
