package com.trade.common.exceptions;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

/**
 * @version 1.0
 * @since 1.0.2
 * @author Jovan Bojovic
 */
@ResponseStatus(code = HttpStatus.NOT_FOUND, reason = "Player not found!")
public class PlayerNotFoundException extends RuntimeException {
}
