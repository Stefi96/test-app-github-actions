package com.trade.common.services;

import com.trade.common.dto.PlayerDataDTO;
import com.trade.common.helper.SessionHelper;
import com.trade.common.interfaces.ConfirmationTokenService;
import com.trade.common.interfaces.PlayerService;
import com.trade.dao.models.ConfirmationToken;
import com.trade.dao.models.Player;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;
import com.trade.dao.repositories.PlayerRepository;
import org.springframework.web.server.ResponseStatusException;

import java.util.Optional;


/**
 * @since 0.0.1
 * @version 1.3
 * @author kocaaa jbojovicct360 AleksaRistovicCT360
 */
@Slf4j
@Service
public class PlayerServiceImpl implements PlayerService {
    @Autowired
    private PlayerRepository playerRepository;
    @Autowired
    private BCryptPasswordEncoder bCryptPasswordEncoder;
    @Autowired
    private ConfirmationTokenService confirmationTokenService;

    /**
     * @param username - string parameter for user's username
     * @param password - string parameter for user's password
     * @return String "Successful login!" if login is successful
     */
    @Override
    public String doLogin(String username, String password) {
        UserDetails userDetails = this.loadUserByUsername(username);
        if (!bCryptPasswordEncoder.matches(password, userDetails.getPassword())) {
            throw new IllegalStateException("Unsuccessful login!");
        }

        Optional<Player> optionalPlayer = playerRepository.findByUsername(username);
        Player player = null;

        if (optionalPlayer.isPresent()) {
            player = optionalPlayer.get();
            if (!player.isEnabled())
                throw new IllegalStateException("Email not confirmed!");
            SessionHelper.sessionSetRole(player.getPlayerRole());
            SessionHelper.sessionSetID(player.getId());
            return "Successful login!";
        }
        throw new IllegalStateException("Unsuccessful login!");
    }

    /**
     * @return PlayerDataDTO object which contains player's username, password and role
     */
    public PlayerDataDTO getPlayerData() {

        long id = SessionHelper.sessionGetID();
        if(id == -1) {
            throw new ResponseStatusException(HttpStatus.FORBIDDEN, "You must be singed in to see user data!");
        }

        Optional<Player> optionalPlayer = playerRepository.findById(id);
        if(optionalPlayer.isEmpty()){
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, "Player not found!");
        }

        Player player = optionalPlayer.get();
        return PlayerDataDTO.PlayerDataDTOBuilder.newInstance()
                .setUsername(player.getUsername())
                .setEmail(player.getEmail())
                .setRole(player.getPlayerRole())
                .build();
    }

    /**
     * @param username - string username for user we want to find
     * @return UserDetails object if user is found
     * @throws UsernameNotFoundException - exception if user isn't found
     */
    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        return playerRepository.findByUsername(username).orElseThrow(() -> new UsernameNotFoundException("No user with that username found."));
    }

    /**
     * @param player - Player object for player we want to sign up
     * @return string value for confirmation token
     */
    @Override
    public String signUpPlayer(Player player){
        log.info("Singing up player with username -> [{}] and with email -> [{}]", player.getUsername(), player.getEmail());
        checkIfEmailIsAvailable(player.getEmail());
        checkIfUsernameIsAvailable(player.getUsername());

        player.setPassword(bCryptPasswordEncoder.encode(player.getPassword()));

        playerRepository.save(player);

        log.info("Creating random generated token for a new player");
        ConfirmationToken confirmationToken = confirmationTokenService.createNewTokenForPlayer(player);
        confirmationTokenService.saveConfirmationToken(confirmationToken);
        log.info("Token -> [{}] has been created", confirmationToken.getToken());

        return confirmationToken.getToken();
    }

    /**
     * @param email - string value of email which should be enabled
     * @return - number of columns changed in database
     */
    @Override
    public Integer enableAppUser(String email) {
        return playerRepository.enablePlayer(email);
    }

    /**
     * @param email - string value of email that needs to be checked if exists in database
     */
    private void checkIfEmailIsAvailable(String email){
        boolean userEmailExists = playerRepository.findByEmail(email).isPresent();


        log.info("Checking if player with email -> [{}] already exist, exception could be thrown", email);
        if(userEmailExists){
            log.warn("Player with email -> [{}] has been found in data base", email);
            throw new IllegalStateException("Email already taken");
        }
    }

    /**
     * @param username - string value of username that needs to be checked if exists in database
     */
    private void checkIfUsernameIsAvailable(String username){
        boolean userUsernameExists = playerRepository.findByUsername(username).isPresent();

        log.info("Checking if player with username -> [{}] already exist, exception could be thrown", username);
        if(userUsernameExists){
            log.warn("Player with username -> [{}] has been found in data base", username);
            throw new IllegalStateException("Username already taken");
        }
    }

}
