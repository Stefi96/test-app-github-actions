package com.trade.common.services;

import com.trade.common.helper.SessionHelper;
import com.trade.common.interfaces.GuestGameService;
import com.trade.dao.models.Guest;
import com.trade.dao.models.GuestGame;
import lombok.Getter;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Slf4j
@Service
public class GuestGameServiceImpl implements GuestGameService {
    @Getter
    private final List<Guest> guestQueue = new ArrayList<>();
    @Getter
    private final List<GuestGame> guestGames = new ArrayList<>();

    @Override
    public synchronized void findGame(){

        GuestGame currentGuestGame = getCurrentGuestGame();
        removeOldGuestGameFromActiveGames(currentGuestGame);

        Guest currentGuest = getCurrentGuest();
        addCurrentGuestToQueue(currentGuest);

        log.trace("Checking how many players are in the queue -> [{}]", guestQueue.size());
        if(guestQueue.size()==2){
            log.info("Game -> [{}] are being created", getCurrentGuestGame());
            createGuestGame();
        }
    }

    @Override
    public GuestGame getCurrentGuestGame() {
        Long currentGuestId = SessionHelper.sessionGetID();
        GuestGame currentGuestGame = null;

        for(GuestGame guestGame : guestGames) {
            if(guestGame.getPlayer1().getId().equals(currentGuestId) || guestGame.getPlayer2().getId().equals(currentGuestId)){
                currentGuestGame = guestGame;
                break;
            }
        }
        return currentGuestGame;
    }

    @Override
    public synchronized void removeCurrentGuestFromQueue(){
        Guest currentGuest = getCurrentGuest();
        log.info("Removing player [{}] from queue", currentGuest.getName());
        guestQueue.remove(currentGuest);
    }

    @Override
    public void playMove(Integer index) {
        GuestGame guestGame = getCurrentGuestGame();

        if(guestGame != null){
            guestGame.playMove(index);
        }
    }

    @Override
    public void refreshGame(){
        GuestGame guestGame = getCurrentGuestGame();
        guestGame.setOpponentRefreshed(true);
    }

    private void removeOldGuestGameFromActiveGames(GuestGame currentGuestGame){
        log.info("Checking if old game [{}] should be deleted from active games.", currentGuestGame);
        if(currentGuestGame != null && currentGuestGame.isOver()){
            log.info("The game -> [{}] has finished and getting closed", currentGuestGame);
            guestGames.remove(currentGuestGame);
        }
    }

    private Guest getCurrentGuest() {

        Guest guest = new Guest();
        guest.setId(SessionHelper.sessionGetID());
        guest.setName(Guest.GUEST_STRING + guest.getId());

        return guest;
    }

    private void addCurrentGuestToQueue(Guest currentGuest){
        log.trace("Checking if player -> [{}] is in the queue", currentGuest.getId());

        if(!guestQueue.contains(currentGuest)){
            log.info("Adding player -> [{}] to a queue", currentGuest.getName());
            guestQueue.add(currentGuest);
        }
    }

    private void createGuestGame() {
        Guest guest1 = guestQueue.get(0);
        Guest guest2 = guestQueue.get(1);

        GuestGame guestGame = new GuestGame(guest1, guest2);

        guestGames.add(guestGame);

        guestQueue.remove(guest1);
        guestQueue.remove(guest2);
    }
}
