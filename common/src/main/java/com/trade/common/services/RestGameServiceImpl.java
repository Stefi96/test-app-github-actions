package com.trade.common.services;

import com.trade.common.dto.GameTimeDTO;
import com.trade.common.exceptions.GameNotFoundException;
import com.trade.common.exceptions.PlayerNotFoundException;
import com.trade.common.helper.SessionHelper;
import com.trade.common.interfaces.PlayerService;
import com.trade.common.interfaces.RestGameService;
import com.trade.dao.models.PlayerGame;
import com.trade.dao.models.FinishedGame;
import com.trade.dao.models.Player;
import com.trade.dao.repositories.FinishedGameRepository;
import com.trade.dao.repositories.PlayerRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

/**
 * @since 0.0.1
 * @version 1.3
 * @author jbojovic
 */
@Slf4j
@Service
public class RestGameServiceImpl implements RestGameService {

    private List<Player> playerQueue = new ArrayList<>();

    private List<PlayerGame> playerGames = new ArrayList<>();

    @Autowired
    private PlayerService playerService;

    @Autowired
    private PlayerRepository playerRepository;

    @Autowired
    private FinishedGameRepository finishedGameRepository;

    /**
     * This method adds player to queue
     *
     * @return String "Player added to queue!" if player is found
     * @throws IllegalArgumentException if player not found
     */
    @Override
    public String addPlayerToQueue() {
    
        Optional<Player> optionalPlayer = playerRepository.findById(SessionHelper.sessionGetID());

        if(optionalPlayer.isEmpty())
            throw new IllegalArgumentException("Player not found!");
        Player player = optionalPlayer.get();

        if(!playerQueue.contains(player)){
            log.info("Adding player -> [{}] to a queue", player.getUsername());
            playerQueue.add(player);
        }

        return "Player added to queue!";
    }

    /**
     * This method create game if there is two
     * players in queue
     */
    @Override
    public synchronized void createGame() {
        if(playerQueue.size() >= 2) {
            Player player1 = playerQueue.get(0);
            Player player2 = playerQueue.get(1);
            playerQueue.remove(player1);
            playerQueue.remove(player2);
            playerGames.add(new PlayerGame(player1, player2));
        }
    }

    /**
     * This method play move that player chose
     *
     * @param id - int value of game
     * @param fieldID - int value of field in the game
     * @return object game to React
     * @throws GameNotFoundException if game isn't found
     */
    @Override
    public synchronized PlayerGame playMove(int id, int fieldID) {
        PlayerGame playerGame = getGameById(id);
        playerGame.playMove(fieldID);
        return playerGame;
    }

    /**
     * This method create game for players
     *
     * @return object game to player if the player is in session
     * @throws GameNotFoundException if game not found
     */
    @Override
    public PlayerGame getGame() {
        createGame();
        long currentPlayerID = SessionHelper.sessionGetID();
        for(PlayerGame playerGame : playerGames) {
            if(playerGame.getPlayer1().getId() == currentPlayerID ||
                    playerGame.getPlayer2().getId() == currentPlayerID)
                return playerGame;
        }
        throw new GameNotFoundException();
    }

    /**
     * @param id - long value of game
     * @return game by game id
     * @throws GameNotFoundException if game not found
     */
    @Override
    public PlayerGame getGameById(long id) {
        for (PlayerGame game : playerGames)
            if(game.getId() == id)
                return game;
        throw new GameNotFoundException();
    }

    /**
     * This method get game id and check witch
     * player turn is on
     *
     * @param id - string value of game id
     * @return witch player is on turn
     */
    @Override
    public Boolean getCurrentPlayerTurn(String id) {
        PlayerGame playerGame = getGameById(Long.valueOf(id));
        return playerGame.getTurn();
    }

    /**
     * @param id - Game ID
     * @return - String if game ended successfully
     * @throws GameNotFoundException if game not found
     */
    @Override
    public String endGame(int id) {
        long playerID = SessionHelper.sessionGetID();
        ArrayList<Integer> result = new ArrayList<>();
        for (int i = 0; i < 14; i++) {
            result.add(0);
        }
        PlayerGame playerGame = getGameById(id);
        if(playerGame.getPlayer1().getId() == playerID) {
            result.add(13, 48);
        } else if (playerGame.getPlayer2().getId() == playerID) {
            result.add(6, 48);
        }
        playerGame.setResult(result);
        return "Game ended successfully!";
    }

    /**
     * @param id - Game ID
     * @return Winner ID (playerID)
     */
    @Override
    public Long deleteGame(int id) {
        PlayerGame playerGame = getGameById(id);
        playerGames.remove(playerGame);
        saveGame(playerGame);

        if(playerGame.getResult().get(6).equals(playerGame.getResult().get(13))){
            return -1L;
        }
        else {
            return playerGame.getResult().get(6) > 24 ? playerGame.getPlayer1().getId() : playerGame.getPlayer2().getId();
        }
    }

    /**
     * @param game - current game object
     */
    private void saveGame(PlayerGame game){
        FinishedGame finishedGame = FinishedGame.builder()
                .player1username(game.getPlayer1().getUsername())
                .player1id(game.getPlayer1().getId())
                .player2username(game.getPlayer2().getUsername())
                .player2id(game.getPlayer2().getId())
                .winnerId(game.getPlayer1().getId()) //player1 is the default winner
                .player1score(game.getResult().get(6))
                .player2score(game.getResult().get(13))
                .startedAt(game.getStartedAt())
                .endedAt(LocalDateTime.now())
                .build();

        if(game.getResult().get(13) > game.getResult().get(6)){
            finishedGame.setWinnerId(game.getPlayer2().getId());
        }
        else if(game.getResult().get(13).equals(game.getResult().get(6))){
            finishedGame.setWinnerId(-1);
        }
        finishedGameRepository.save(finishedGame);
    }

    /**
     * @return List of all games that current player have played
     */
    @Override
    public List<FinishedGame> getHistoryOfCurrentUser() {
        Long id = SessionHelper.sessionGetID();
        return finishedGameRepository.getHistoryById(id);
    }

    /**
     * @param id - GameID
     * @return GameTimeDTO which contains time of both players in game
     */
    @Override
    public GameTimeDTO getTime(long id) {
        PlayerGame game = getGameById(id);
        return new GameTimeDTO(game.getPlayer1().getId(), game.getPlayerOneTime(), game.getPlayer2().getId(), game.getPlayerTwoTime());
    }

    /**
     * @param id - GameID
     * @return remaining time for current player decreased by 1
     */
    @Override
    public Integer decreaseMyTime(long id) {
        PlayerGame playerGame = getGameById(id);
        long currentPlayerID = SessionHelper.sessionGetID();
        Integer currentTime;
        if (playerGame.getPlayer1().getId() == currentPlayerID) {
            currentTime = playerGame.getPlayerOneTime() - 1;
            playerGame.setPlayerOneTime(currentTime);
        } else {
            currentTime = playerGame.getPlayerTwoTime() - 1;
            playerGame.setPlayerTwoTime(currentTime);
        }
        return currentTime;
    }

}
