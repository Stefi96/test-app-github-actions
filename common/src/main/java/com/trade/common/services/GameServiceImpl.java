package com.trade.common.services;

import com.trade.common.interfaces.GameService;
import com.trade.dao.models.FinishedGame;
import com.trade.dao.models.PlayerGame;
import com.trade.dao.models.LeaderboardEntry;
import com.trade.dao.models.Player;
import com.trade.dao.repositories.FinishedGameRepository;
import com.trade.dao.repositories.PlayerRepository;
import lombok.Getter;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

/**
 * @since 1.0.1
 * @version 1.3
 * @author kocaaa martel7 jbojovicct360 Aleksa
 */
@Slf4j
@Service
public class GameServiceImpl implements GameService {

    @Getter
    private final List<Player> playerQueue = new ArrayList<>();
    @Getter
    private final List<PlayerGame> playerGames = new ArrayList<>();
    @Autowired
    private FinishedGameRepository finishedGameRepository;
    @Autowired
    private PlayerRepository playerRepository;

    /**
     * Method is finishing game,
     * adding player in queue,
     * and creating game
     */
    @Override
    public synchronized void findGame(){

        PlayerGame currentPlayerGame = getCurrentGame();
        removeOldGameFromActiveGames(currentPlayerGame);

        Player currentPlayer = getCurrentPlayer();
        addCurrentPlayerToQueue(currentPlayer);

        log.trace("Checking how many players are in the queue -> [{}]", playerQueue.size());
        if(playerQueue.size()==2){
            log.info("Game -> [{}] are being created", getCurrentGame());
            createGame();
        }
    }

    /**
     * Method is stopping game,
     * and removing players from it
     */
    @Override
    public void removeCurrentPlayerFromQueue(){
        Player currentPlayer = getCurrentPlayer();
        log.info("Removing player [{}] from queue", currentPlayer.getUsername());
        playerQueue.remove(currentPlayer);
    }

    /**
     * Method is playing a move and saving info about the game if it's finished
     *
     * @param index - index of field in Mancala
     */
    @Override
    public void playMove(Integer index) {
        PlayerGame playerGame = getCurrentGame();

        if(playerGame != null){
            playerGame.playMove(index);
        }

        if(playerGame != null && playerGame.isOver()){
            saveGame();
        }
    }

    /**
     *  Method refreshes game for both players
     */
    @Override
    public void refreshGame(){
        PlayerGame playerGame = getCurrentGame();
        playerGame.setOpponentRefreshed(true);
    }

    /**
     * @return Game object for current game
     */
    @Override
    public PlayerGame getCurrentGame() {
        Long currentPlayerId = getCurrentPlayer().getId();
        PlayerGame currentPlayerGame = null;

        for(PlayerGame playerGame : playerGames) {
            if (playerGame.getPlayer1().getId().equals(currentPlayerId) || playerGame.getPlayer2().getId().equals(currentPlayerId)) {
                currentPlayerGame = playerGame;
                break;
            }
        }

        return currentPlayerGame;
    }

    /**
     *  Method is creating game for first two player in queue
     */
    private void createGame() {
        Player player1 = playerQueue.get(0);
        Player player2 = playerQueue.get(1);

        PlayerGame playerGame = new PlayerGame(player1, player2);

        playerGames.add(playerGame);

        playerQueue.remove(player1);
        playerQueue.remove(player2);
    }

    /**
     * @param currentPlayerGame - game that current player is playing
     * Method checks if there is any old game that
     */
    private void removeOldGameFromActiveGames(PlayerGame currentPlayerGame){
        log.info("Checking if old game [{}] should be deleted from active games.", currentPlayerGame);
        if(currentPlayerGame != null && currentPlayerGame.isOver()){
            log.info("The game -> [{}] has finished and getting closed", currentPlayerGame);
            playerGames.remove(currentPlayerGame);
        }
    }

    /**
     * @param currentPlayer - current player that is logged in
     * Method is adding that player into queue for game
     */
    private void addCurrentPlayerToQueue(Player currentPlayer){
        log.trace("Checking if player -> [{}] is in the queue", currentPlayer.getUsername());
        if(!playerQueue.contains(currentPlayer)){
            log.info("Adding player -> [{}] to a queue", currentPlayer.getUsername());
            playerQueue.add(currentPlayer);
        }
    }

    /**
     * @return Player object for current player
     */
    private Player getCurrentPlayer() {
        return (Player) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
    }

    /**
     * Saving info about a finished game into the DB
     */
    private void saveGame(){
        PlayerGame playerGame = getCurrentGame();

        FinishedGame finishedGame = new FinishedGame(
                playerGame.getId(),
                playerGame.getPlayer1().getUsername(),
                playerGame.getPlayer1().getId(),
                playerGame.getPlayer2().getUsername(),
                playerGame.getPlayer2().getId(),
                playerGame.getPlayer1().getId(), //player1 is the default winner
                playerGame.getResult().get(6),
                playerGame.getResult().get(13),
                playerGame.getStartedAt(),
                LocalDateTime.now()
        );

        if(playerGame.getResult().get(13) > playerGame.getResult().get(6)){
            finishedGame.setWinnerId(playerGame.getPlayer2().getId());
        }
        else if(playerGame.getResult().get(13).equals(playerGame.getResult().get(6))){
            finishedGame.setWinnerId(-1);
        }



        finishedGameRepository.save(finishedGame);
    }

    /**
     *
     * @return list of leaderboard entries that form the Leaderboard
     */
    @Override
    public List<LeaderboardEntry> getLeaderboard() {
        List<LeaderboardEntry> leaderboard = finishedGameRepository.getLeaderboard();
        Optional<Player> playerOptional;

        for(LeaderboardEntry leaderboardEntry : leaderboard){
            playerOptional = playerRepository.findById(leaderboardEntry.getUserID());
            if(playerOptional.isPresent()){
                leaderboardEntry.setUsername(playerOptional.get().getUsername());
            }
        }

        return leaderboard;
    }

    /**
     *
     * @return - List of finished game objects for the specific player that form his history of games
     */
    @Override
    public List<FinishedGame> getHistoryOfCurrentUser() {
        Long id = getCurrentPlayer().getId();

        return finishedGameRepository.getHistoryById(id);
    }
}