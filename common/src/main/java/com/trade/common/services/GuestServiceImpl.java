package com.trade.common.services;

import com.trade.common.helper.SessionHelper;
import com.trade.common.interfaces.GuestService;
import com.trade.dao.enums.PlayerRole;
import com.trade.dao.models.Guest;
import org.springframework.stereotype.Service;

@Service
public class GuestServiceImpl implements GuestService {
    @Override
    public void loginNewGuest() {
        Guest guest = new Guest();
        SessionHelper.sessionSetID(guest.getId());
        SessionHelper.sessionSetRole(PlayerRole.GUEST);
    }
}
