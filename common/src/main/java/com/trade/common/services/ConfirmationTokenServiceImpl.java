package com.trade.common.services;

import com.trade.common.interfaces.ConfirmationTokenService;
import com.trade.dao.models.ConfirmationToken;
import com.trade.dao.models.Player;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.trade.dao.repositories.ConfirmationTokenRepository;

import java.time.LocalDateTime;
import java.util.Optional;
import java.util.UUID;

/**
 * @author Koca , Aleksa
 * @since 0.0.1
 * @version 1.2
 */
@Service
public class ConfirmationTokenServiceImpl implements ConfirmationTokenService {

    private static final Logger logger = LogManager.getLogger(ConfirmationTokenServiceImpl.class);

    @Autowired
    private ConfirmationTokenRepository confirmationTokenRepository;

    /**
     * This method save confirmation token for a player
     *
     * @param token - token for user authentication
     */
    @Override
    public void saveConfirmationToken(ConfirmationToken token) {
        confirmationTokenRepository.save(token);
    }

    /**
     * This method find token by token in DB
     *
     * @param token - token for user authentication
     * @return String token value
     */
    @Override
    public Optional<ConfirmationToken> getToken(String token) {
        return confirmationTokenRepository.findByToken(token);
    }

    /**
     * This method update token to set it as confirmed in DB
     *
     * @param token - token for user authentication
     * @return String token value
     */
    @Override
    public Integer setConfirmedAt(String token) {
        return confirmationTokenRepository.updateConfirmedAt(token, LocalDateTime.now());
    }

    /**
     * This method creates new random generated token for
     * newly registered player
     *
     * @param player  - object with player's data
     * @return ConfirmationToken for player
     */
    @Override
    public ConfirmationToken createNewTokenForPlayer(Player player) {
        logger.info("Creating random generated token for a player -> [{}] ", player.getUsername());
        String token = UUID.randomUUID().toString();
        logger.info("Random token is generated for a player -> [{}] , and token is being stored as ConfirmationToken.", player.getUsername());

        return new ConfirmationToken(
                token,
                LocalDateTime.now(),
                LocalDateTime.now().plusMinutes(ConfirmationToken.TOKEN_DURATION),
                player
        );
    }
}
