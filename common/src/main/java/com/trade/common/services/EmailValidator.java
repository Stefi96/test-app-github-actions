package com.trade.common.services;

import org.springframework.stereotype.Service;

import java.util.function.Predicate;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * @since 0.0.1
 * @version 1.0
 * @author Koca
 */
@Service
public class EmailValidator implements Predicate<String> {
    private static final Pattern VALID_EMAIL_ADDRESS_REGEX = Pattern.compile("^[A-Z0-9._%+-]+@[A-Z0-9.-]+\\.[A-Z]{2,6}$", Pattern.CASE_INSENSITIVE);

    /**
     * This method validates that email is in email format
     *
     * @param emailStr - string value for email to be checked
     * @return true if string can be email, false if it can't
     */
    private static boolean validateEmailRegex(String emailStr) {
        Matcher matcher = VALID_EMAIL_ADDRESS_REGEX.matcher(emailStr);
        return matcher.find();
    }

    /**
     * @param email - string to check if it can be email
     * @return true if string can be email, false if it can't be email
     */
    @Override
    public boolean test(String email) {
        return EmailValidator.validateEmailRegex(email);
    }
}
