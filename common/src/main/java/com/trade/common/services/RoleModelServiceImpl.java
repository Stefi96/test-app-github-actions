package com.trade.common.services;

import com.trade.common.interfaces.RoleModelService;
import com.trade.dao.enums.PlayerRole;
import org.springframework.stereotype.Service;
import org.springframework.ui.Model;

@Service
public class RoleModelServiceImpl implements RoleModelService {
    @Override
    public void addRoleModel(Model model, PlayerRole playerRole){
        model.addAttribute("role", playerRole.toString());
    }
}
