package com.trade.common.helper;

import com.trade.dao.enums.PlayerRole;
import org.springframework.web.context.request.RequestAttributes;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import javax.servlet.http.HttpSession;

/**
 * @since 1.0.2
 * @version 1.0.2
 * @author jbojovic
 */
public class SessionHelper {

    /**
     * @param role - PlayerRole object for setting player role in session
     */
    public static void sessionSetRole(PlayerRole role) {
        SessionHelper.getSession().setAttribute("role", role);
    }

    /**
     * @param id - long object for setting player id in session
     */
    public static void sessionSetID(long id) {
        SessionHelper.getSession().setAttribute("id", id);
    }

    /**
     * @return long value of current player's id from session
     */
    public static long sessionGetID() {
        Object objectID = SessionHelper.getSession().getAttribute("id");
        return objectID != null ? (long) objectID : -1;
    }

    private static HttpSession getSession() {
        ServletRequestAttributes servletRequestAttributes = (ServletRequestAttributes) RequestContextHolder.currentRequestAttributes();
        return servletRequestAttributes.getRequest().getSession();
    }
}
