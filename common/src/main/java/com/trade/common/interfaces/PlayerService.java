package com.trade.common.interfaces;

import com.trade.common.dto.PlayerDataDTO;
import com.trade.dao.models.Player;
import org.springframework.security.core.userdetails.UserDetailsService;

/**
 * PlayerService interface
 * is implemented in PlayerServiceImpl
 *
 * @author Koca
 * @since 0.0.1
 * @version 1.2
 */
public interface PlayerService extends UserDetailsService {

    String signUpPlayer(Player player);
    Integer enableAppUser(String email);

    String doLogin(String username, String password);

    PlayerDataDTO getPlayerData();

}
