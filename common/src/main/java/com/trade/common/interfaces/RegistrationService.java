package com.trade.common.interfaces;

import com.trade.common.dto.RegistrationRequest;

import javax.transaction.Transactional;

/**
 * RegistrationService interface
 * is implemented in RegistrationServiceImpl
 *
 * @author Koca
 * @since 0.0.1
 * @version 1.2
 */
public interface RegistrationService {

    String register(RegistrationRequest request);
    @Transactional
    String confirmToken(String token);
}
