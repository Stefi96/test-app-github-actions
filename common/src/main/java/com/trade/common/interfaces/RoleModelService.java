package com.trade.common.interfaces;

import com.trade.dao.enums.PlayerRole;
import org.springframework.ui.Model;

public interface RoleModelService {
    void addRoleModel(Model model, PlayerRole playerRole);
}
