package com.trade.common.interfaces;

public interface EmailSenderService {
    void send(String to, String email);
}
