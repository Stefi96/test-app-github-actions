package com.trade.common.interfaces;

import com.trade.dao.models.ConfirmationToken;
import com.trade.dao.models.Player;

import java.util.Optional;

/**
 * ConfirmationTokenService interface
 * is implemented in ConfirmationTokenServiceImpl
 *
 * @author Koca
 * @since 0.0.1
 * @version 1.2
 */
public interface ConfirmationTokenService {
    void saveConfirmationToken(ConfirmationToken token);
    Optional<ConfirmationToken> getToken(String token);
    Integer setConfirmedAt(String token);
    ConfirmationToken createNewTokenForPlayer(Player player);
}
