package com.trade.common.interfaces;

import com.trade.common.dto.GameTimeDTO;
import com.trade.dao.models.PlayerGame;
import com.trade.dao.models.FinishedGame;

import java.util.List;

/**
 * RestGameService interface
 * is implemented in RestGameServiceImpl
 *
 * @author Jovan Bojovic
 * @since 0.0.1
 * @version 1.2
 */
public interface RestGameService {

    String addPlayerToQueue();
    void createGame();
    PlayerGame playMove(int id, int fieldID);

    PlayerGame getGame();

    PlayerGame getGameById(long id);

    Boolean getCurrentPlayerTurn(String id);

    String endGame(int id);

    Long deleteGame(int id);

    List<FinishedGame> getHistoryOfCurrentUser();

    GameTimeDTO getTime(long id);

    Integer decreaseMyTime(long id);

}
