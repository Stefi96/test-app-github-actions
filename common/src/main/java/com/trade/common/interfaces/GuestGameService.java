package com.trade.common.interfaces;

import com.trade.dao.models.GuestGame;

public interface GuestGameService {
    GuestGame getCurrentGuestGame();
    void findGame();
    void removeCurrentGuestFromQueue();
    void playMove(Integer index);
    void refreshGame();
}
