package com.trade.common.interfaces;

import com.trade.dao.models.FinishedGame;
import com.trade.dao.models.PlayerGame;
import com.trade.dao.models.LeaderboardEntry;

import java.util.List;

/**
 *  GameService interface
 *  is implemented in GameServiceImpl
 *
 * @author Dusan Markovic
 * @since 0.0.1
 * @version 1.2
 */
public interface GameService {

    PlayerGame getCurrentGame();
    void findGame();
    void removeCurrentPlayerFromQueue();
    void playMove(Integer index);
    void refreshGame();
    List<LeaderboardEntry> getLeaderboard();
    List<FinishedGame> getHistoryOfCurrentUser();
}
