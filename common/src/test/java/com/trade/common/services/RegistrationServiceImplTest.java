package com.trade.common.services;

import com.trade.common.dto.RegistrationRequest;
import com.trade.common.interfaces.ConfirmationTokenService;
import com.trade.common.interfaces.PlayerService;
import com.trade.common.interfaces.RegistrationService;
import com.trade.dao.enums.PlayerRole;
import com.trade.dao.models.ConfirmationToken;
import com.trade.dao.models.Player;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.time.LocalDateTime;
import java.util.Optional;

import static org.mockito.Mockito.lenient;
import static org.mockito.Mockito.when;

import static org.junit.jupiter.api.Assertions.*;

@ExtendWith(MockitoExtension.class)
class RegistrationServiceImplTest {

    @InjectMocks
    RegistrationService registrationService = new RegistrationServiceImpl();
    @Mock
    EmailValidator emailValidator;
    @Mock
    PlayerService playerService;
    @Mock
    ConfirmationTokenService confirmationTokenService;

    private RegistrationRequest request;

    private Player player;

    private String token;

    private ConfirmationToken confirmationToken;

    @BeforeEach
    void setUp() {
        player = new Player("player", "player@player.com", "player", PlayerRole.USER);
        player.setEnabled(true);
        request = new RegistrationRequest("player", "player@player.com", "player");
        token = "387e6930-0f59-4ed1-98a9-2bfa69765300";
        LocalDateTime now = LocalDateTime.now();
        LocalDateTime expires = now.plusDays(1);
        confirmationToken = new ConfirmationToken("387e6930-0f59-4ed1-98a9-2bfa69765300", now, expires, player);

    }

    @Test
    void register() {

            when(emailValidator.test("player@player.com")).thenReturn(true);
            lenient().when(playerService.signUpPlayer(player)).thenReturn(token);
            assertEquals(token, registrationService.register(request));

    }

    @Test
    void confirmToken() {

        when(confirmationTokenService.getToken(token)).thenReturn(Optional.of(confirmationToken));
        when(confirmationTokenService.setConfirmedAt(token)).thenReturn(1);
        when(playerService.enableAppUser("player@player.com")).thenReturn(1);

        assertEquals("Confirmed", registrationService.confirmToken(token));

    }
}