package com.trade.common.services;

import com.trade.common.interfaces.ConfirmationTokenService;
import com.trade.common.interfaces.PlayerService;
import com.trade.dao.enums.PlayerRole;
import com.trade.dao.models.ConfirmationToken;
import com.trade.dao.models.Player;
import com.trade.dao.repositories.PlayerRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

import java.time.LocalDateTime;
import java.util.Optional;
import java.util.UUID;

import static org.mockito.Mockito.when;

import static org.junit.jupiter.api.Assertions.*;

@ExtendWith(MockitoExtension.class)
class PlayerServiceImplTest {

    @InjectMocks
    PlayerService service = new PlayerServiceImpl();

    @Mock
    PlayerRepository repository;

    @Mock
    ConfirmationTokenService confirmationTokenService;

    @Mock
    BCryptPasswordEncoder bCryptPasswordEncoder;

    private Player player;

    private ConfirmationToken token;

    @BeforeEach
    void setUp() {
        player = new Player("player", "player@player.com", "player", PlayerRole.USER);
        LocalDateTime time = LocalDateTime.now();
        token = new ConfirmationToken(UUID.randomUUID().toString(), time, time, player);
    }

    @Test
    void loadUserByUsername() {
        when(repository.findByUsername("player")).thenReturn((Optional<Player>) Optional.of(player));
        assertEquals(service.loadUserByUsername("player").getUsername(), player.getUsername());
    }

    @Test
    void signUpPlayer() {
        when(repository.findByEmail("player@player.com")).thenReturn(Optional.empty());
        when(repository.findByUsername("player")).thenReturn(Optional.empty());
        when(repository.save(player)).thenReturn(player);
        when(bCryptPasswordEncoder.encode("player")).thenReturn("player");
        when(confirmationTokenService.createNewTokenForPlayer(player)).thenReturn(token);
        assertEquals(token.getToken(), service.signUpPlayer(player));
    }

    @Test
    void enableAppUser() {
        when(repository.enablePlayer("player@player.com")).thenReturn(1);
        assertEquals(1, service.enableAppUser(player.getEmail()));
    }
}