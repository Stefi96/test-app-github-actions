package com.trade.common.services;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class EmailValidatorTest {

    EmailValidator emailValidator;

    @BeforeEach
    void setUp() {
        emailValidator = new EmailValidator();
    }

    @Test
    void validateEmailRegex() {
        assertEquals(true, emailValidator.test("jovan.bojovic@comtrade.com"));
    }

    @Test
    void test1() {
        assertEquals(true, emailValidator.test("jovan.bojovic@comtrade.com"));
    }
}