package com.trade.common.services;

import com.trade.dao.models.FinishedGame;
import com.trade.dao.models.PlayerGame;
import com.trade.dao.models.LeaderboardEntry;
import com.trade.dao.models.Player;
import com.trade.dao.repositories.FinishedGameRepository;
import com.trade.dao.repositories.PlayerRepository;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.Spy;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.context.SecurityContextHolder;

import java.util.*;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
class PlayerGameServiceImplTest {
    @Spy
    @InjectMocks
    private GameServiceImpl gameServiceImpl;
    @Mock
    private FinishedGameRepository finishedGameRepository;
    @Mock
    private PlayerRepository playerRepository;
    @Mock
    private SecurityContextHolder securityContextHolder;
    private static Player player1, player2;
    private static PlayerGame playerGame;

    @BeforeAll
    public static void setup(){
        Authentication authentication = Mockito.mock(Authentication.class);
        SecurityContext securityContext = Mockito.mock(SecurityContext.class);
        when(securityContext.getAuthentication()).thenReturn(authentication);
        SecurityContextHolder.setContext(securityContext);

        player1 = new Player();
        player1.setUsername("dusan7");
        player1.setId(1L);

        player2 = new Player();
        player2.setUsername("koca");
        player2.setId(2L);

        when(SecurityContextHolder.getContext().getAuthentication().getPrincipal()).thenReturn(player1);

        playerGame = new PlayerGame(player1, player2);
    }

    @Test
    void removeCurrentPlayerFromQueue() {
        gameServiceImpl.removeCurrentPlayerFromQueue();
        assertEquals(Collections.emptyList(), gameServiceImpl.getPlayerQueue());
    }

    @Test
    void playMove_gameNotOver() {
        when(gameServiceImpl.getCurrentGame()).thenReturn(playerGame);
        gameServiceImpl.playMove(0);

        List<Integer> expectedList = new ArrayList<>(Arrays.asList(0,5,5,5,5,4,0,4,4,4,4,4,4,0));

        assertEquals(expectedList, gameServiceImpl.getCurrentGame().getResult());
    }

    @Test
    void playMove_gameOver_player2Winner() {
        //when(game.isOver()).thenReturn(true);
        PlayerGame playerGame = Mockito.mock(PlayerGame.class);
        when(playerGame.getId()).thenReturn(1);
        when(playerGame.getPlayer1()).thenReturn(player1);
        when(playerGame.getPlayer2()).thenReturn(player2);
        when(playerGame.getResult()).thenReturn(new ArrayList<>(Arrays.asList(0,5,5,5,5,4,0,4,4,4,4,4,4,10)));
        when(playerGame.isOver()).thenReturn(true);

        when(gameServiceImpl.getCurrentGame()).thenReturn(playerGame);

        List<Integer> expectedList = new ArrayList<>(Arrays.asList(0,5,5,5,5,4,0,4,4,4,4,4,4,10));

        gameServiceImpl.playMove(0);

        assertEquals(expectedList, gameServiceImpl.getCurrentGame().getResult());
    }

    @Test
    void playMove_gameOver_Tie() {
        //when(game.isOver()).thenReturn(true);
        PlayerGame playerGame = Mockito.mock(PlayerGame.class);
        when(playerGame.getId()).thenReturn(1);
        when(playerGame.getPlayer1()).thenReturn(player1);
        when(playerGame.getPlayer2()).thenReturn(player2);
        when(playerGame.getResult()).thenReturn(new ArrayList<>(Arrays.asList(0,5,5,5,5,4,10,4,4,4,4,4,4,10)));
        when(playerGame.isOver()).thenReturn(true);

        when(gameServiceImpl.getCurrentGame()).thenReturn(playerGame);

        List<Integer> expectedList = new ArrayList<>(Arrays.asList(0,5,5,5,5,4,10,4,4,4,4,4,4,10));

        gameServiceImpl.playMove(0);

        assertEquals(expectedList, gameServiceImpl.getCurrentGame().getResult());
    }

    @Test
    void refreshGame() {
        when(gameServiceImpl.getCurrentGame()).thenReturn(playerGame);

        gameServiceImpl.refreshGame();

        assertEquals(true, gameServiceImpl.getCurrentGame().isOpponentRefreshed());
    }

    @Test
    void getCurrentGame() {
        when(gameServiceImpl.getCurrentGame()).thenReturn(playerGame);
        System.out.println(gameServiceImpl.getCurrentGame());
        assertEquals(playerGame, gameServiceImpl.getCurrentGame());
    }

    @Test
    void getLeaderboard() {
        List<LeaderboardEntry> leaderboard = new ArrayList<>();
        leaderboard.add(new LeaderboardEntry("dusan7", 1, 5));
        leaderboard.add(new LeaderboardEntry("koca", 2, 3));
        leaderboard.add(new LeaderboardEntry("jovan", 3, 1));
        leaderboard.add(new LeaderboardEntry("aleksaR", 4, 4));

        //https://www.baeldung.com/mockito-unnecessary-stubbing-exception
        lenient().when(finishedGameRepository.getLeaderboard()).thenReturn(leaderboard);

        lenient().when(playerRepository.findById(1L)).thenReturn(Optional.of(player1));

        assertEquals(leaderboard, gameServiceImpl.getLeaderboard());
    }

    @Test
    void getHistory() {
        List<FinishedGame> history = new ArrayList<>();
        FinishedGame finishedGame1 = new FinishedGame();
        FinishedGame finishedGame2 = new FinishedGame();
        history.add(finishedGame1);
        history.add(finishedGame2);

        when(gameServiceImpl.getHistoryOfCurrentUser()).thenReturn(history);

        assertEquals(2, gameServiceImpl.getHistoryOfCurrentUser().size());

        verify(gameServiceImpl, times(1)).getHistoryOfCurrentUser();
    }
}