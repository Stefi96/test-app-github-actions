package com.trade.common.services;

import com.trade.common.interfaces.ConfirmationTokenService;
import com.trade.dao.enums.PlayerRole;
import com.trade.dao.models.ConfirmationToken;
import com.trade.dao.models.Player;
import com.trade.dao.repositories.ConfirmationTokenRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockedStatic;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;
import java.time.LocalDateTime;
import java.util.Optional;
import java.util.UUID;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
class ConfirmationTokenServiceImplTest {

    @InjectMocks
    private ConfirmationTokenService service = new ConfirmationTokenServiceImpl();

    @Mock
    private ConfirmationTokenRepository repository;

    private ConfirmationToken token;

    private Player player;

    @BeforeEach
    public void setUp() {
        player = new Player("player", "player@player.com", "player", PlayerRole.USER);
        token = service.createNewTokenForPlayer(player);
    }

    @Test
    void getToken() {

        Optional<ConfirmationToken> tokenOptional = Optional.of(token);
        when(repository.findByToken(token.getToken())).thenReturn(tokenOptional);
        Optional<ConfirmationToken> tokenFromService = service.getToken(token.getToken());
        assertEquals(token.getToken(), tokenFromService.get().getToken());

    }

    @Test
    void setConfirmedAt() {

        LocalDateTime time = LocalDateTime.now();
        try(MockedStatic<LocalDateTime> mockedStatic = Mockito.mockStatic(LocalDateTime.class)){
            when(LocalDateTime.now()).thenReturn(time);
            when(repository.updateConfirmedAt(token.getToken(), time)).thenReturn(1);
            assertEquals(1, service.setConfirmedAt(token.getToken()));
        }

    }

    @Test
    void createNewTokenForPlayer() {

        UUID uuid = UUID.randomUUID();
        try (MockedStatic<UUID> mockedStatic = Mockito.mockStatic(UUID.class)) {;
            when(UUID.randomUUID()).thenReturn(uuid);
            ConfirmationToken generatedToken = service.createNewTokenForPlayer(player);
            assertEquals(generatedToken.getToken(), uuid.toString());
        }

    }
}