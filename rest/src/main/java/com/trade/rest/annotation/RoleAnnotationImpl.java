package com.trade.rest.annotation;

import com.trade.dao.enums.PlayerRole;
import com.trade.rest.exception.PlayerUnauthorizedException;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Pointcut;
import org.aspectj.lang.reflect.MethodSignature;
import org.springframework.stereotype.Component;
import org.springframework.web.context.request.RequestAttributes;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import javax.servlet.http.HttpServletRequest;

/**
 * @since 1.0.2
 * @version 1.0
 * @author jbojovic
 */
@Aspect
@Component
public class RoleAnnotationImpl {

    @Pointcut(value = "execution(* *.*(..))")
    public void allMethods() {}

    /**
     * @param joinPoint - ProceedingJoinPoint object
     * @return object which function is using
     * @throws Throwable
     */
    @Around("@annotation(Role)")
    public Object roleImplementation(ProceedingJoinPoint joinPoint) throws Throwable {
        Object proceed = joinPoint.proceed();
        Role role = ((MethodSignature) joinPoint.getSignature()).getMethod().getAnnotation(Role.class);
        PlayerRole roles[] = role.role();
        RequestAttributes requestAttributes = RequestContextHolder.currentRequestAttributes();
        ServletRequestAttributes servletRequestAttributes = (ServletRequestAttributes) requestAttributes;
        HttpServletRequest request = servletRequestAttributes.getRequest();
        PlayerRole currentPlayerRole = (PlayerRole) request.getSession().getAttribute("role");
        boolean isAuthorized = false;
        for (int i = 0; i < roles.length && !isAuthorized; i++)
            if (roles[i].equals(currentPlayerRole))
                isAuthorized = true;
        if(!isAuthorized)
            throw new PlayerUnauthorizedException();
        return proceed;
    }

}
