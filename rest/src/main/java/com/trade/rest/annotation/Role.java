package com.trade.rest.annotation;

import com.trade.dao.enums.PlayerRole;

import java.lang.annotation.*;

@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.METHOD)
@Documented
public @interface Role {

    PlayerRole[] role() default PlayerRole.GUEST;

}
