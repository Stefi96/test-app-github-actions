package com.trade.rest.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.web.configuration.WebSecurityCustomizer;

/**
 * @since 1.0.2
 * @version 1.0
 * @author jbojovic
 */
@Configuration
public class SecurityConfig {

    /**
     * @return WebSecurityCustomizer object for customizing Spring Security
     */
    @Bean
    public WebSecurityCustomizer webSecurityCustomizer() {
        return (web) -> web.ignoring().antMatchers("/**");
    }

}
