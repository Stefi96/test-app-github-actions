package com.trade.rest.contoller;

import com.trade.common.dto.GameTimeDTO;
import com.trade.common.interfaces.GameService;
import com.trade.common.interfaces.RestGameService;
import com.trade.dao.models.PlayerGame;
import com.trade.dao.models.LeaderboardEntry;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/game")
@CrossOrigin(origins = "http://localhost:3000", allowCredentials = "true")
public class GameController {

    @Autowired
    private RestGameService restGameService;

    @Autowired
    private GameService gameService;

    @GetMapping("/queue")
    public ResponseEntity<String> playerToQueue() {
        return new ResponseEntity<>(restGameService.addPlayerToQueue(), HttpStatus.OK);
    }

    @GetMapping("/create-game")
    public ResponseEntity<PlayerGame> createGame() {
        return new ResponseEntity<>(restGameService.getGame(), HttpStatus.OK);
    }

    @GetMapping("/game={gameID}&field={fieldID}")
    public ResponseEntity<PlayerGame> playMove(@PathVariable("gameID") int gameID, @PathVariable("fieldID") int fieldID) {
        return new ResponseEntity<>(restGameService.playMove(gameID, fieldID), HttpStatus.OK);
    }

    @GetMapping("/id={id}")
    public ResponseEntity<PlayerGame> getGameById(@PathVariable("id") long id) {
        return new ResponseEntity<>(restGameService.getGameById(id), HttpStatus.OK);
    }

    @GetMapping("/turn/id={id}")
    public ResponseEntity<Boolean> getCurrentPlayerTurn(@PathVariable("id") String id) {
        return new ResponseEntity<>(restGameService.getCurrentPlayerTurn(id), HttpStatus.OK);
    }

    @GetMapping("/end/id={id}")
    public ResponseEntity<String> endGame(@PathVariable("id") int id) {
        return new ResponseEntity<>(restGameService.endGame(id), HttpStatus.OK);
    }

    @GetMapping("/leaderboard")
    public ResponseEntity<List<LeaderboardEntry>> getLeaderboard() {
        return new ResponseEntity<>(gameService.getLeaderboard(), HttpStatus.OK);
    }

    @DeleteMapping("id={id}")
    public ResponseEntity<Long> deleteGame(@PathVariable int id) {
        return new ResponseEntity(restGameService.deleteGame(id), HttpStatus.OK);
    }

    @GetMapping("/time/game={id}")
    public ResponseEntity<GameTimeDTO> getGameTime(@PathVariable long id) {
        return new ResponseEntity<>(restGameService.getTime(id), HttpStatus.OK);
    }

    @GetMapping("/decrease-time/id={id}")
    public ResponseEntity<Integer> myTime(@PathVariable long id) {
        return new ResponseEntity<>(restGameService.decreaseMyTime(id), HttpStatus.OK);
    }

}
