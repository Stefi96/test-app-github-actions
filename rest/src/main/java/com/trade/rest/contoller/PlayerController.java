package com.trade.rest.contoller;

import com.trade.common.dto.LoginDTO;
import com.trade.common.dto.PlayerDataDTO;
import com.trade.common.dto.RegistrationRequest;
import com.trade.common.exceptions.EmailTakenException;
import com.trade.common.exceptions.UnsuccessfulLoginException;
import com.trade.common.exceptions.UsernameTakenException;
import com.trade.common.helper.SessionHelper;
import com.trade.common.interfaces.ConfirmationTokenService;
import com.trade.common.interfaces.PlayerService;
import com.trade.common.interfaces.RegistrationService;
import com.trade.common.interfaces.RestGameService;
import com.trade.dao.enums.PlayerRole;
import com.trade.dao.models.FinishedGame;
import com.trade.rest.annotation.Role;
import com.trade.rest.exception.EmailConfirmedException;
import com.trade.rest.exception.EmailNotConfirmedException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

/**
 * @since 1.0.1
 * @version 1.0
 * @author jbojovic
 */
@RestController
@CrossOrigin(origins = "http://localhost:3000", allowCredentials = "true")
public class PlayerController {

    @Autowired
    private PlayerService playerService;

    @Autowired
    private RegistrationService registrationService;

    @Autowired
    private RestGameService restGameService;

    /**
     * @param request - HttpServletRequest object
     */
    @GetMapping("/")
    public void setRole(HttpServletRequest request) {
        request.getSession().setAttribute("role", PlayerRole.USER);
    }

    /**
     * @param request - RegistrationRequest object which contains username, email and password
     * @return string with user's confirmation token
     */
    @PostMapping("/register")
    public String register (@RequestBody RegistrationRequest request){
        String registrationServiceString = "";
        try {
            registrationServiceString = registrationService.register(request);
        } catch (IllegalStateException e) {
            if(e.getMessage().equals("Email already taken"))
                throw new EmailTakenException();
            if(e.getMessage().equals("Username already taken"))
                throw new UsernameTakenException();
        }
        return registrationServiceString;
    }

    /**
     * @param loginDTO - LoginDTO object which contains player's username and password
     * @return ResponseEntity with OK status and String message "Successful login!"
     */
    @PostMapping("/login")
    public ResponseEntity<String> login(@RequestBody LoginDTO loginDTO) {
        String message = "";
        try {
            message = playerService.doLogin(loginDTO.getUsername(), loginDTO.getPassword());
        } catch (IllegalStateException e) {
            String exceptionMessage = e.getMessage();
            if(exceptionMessage.equals("Unsuccessful login!"))
                throw new UnsuccessfulLoginException();
            if(exceptionMessage.equals("Email not confirmed!"))
                throw new EmailNotConfirmedException();
        }
        return new ResponseEntity<>(message, HttpStatus.OK);
    }

    /**
     * @param request - HttpServletRequest with request sent
     * @return ResponseEntity with OK status and message "You are successfully logged out"
     */
    @GetMapping("/logout")
    public ResponseEntity<String> logout(HttpServletRequest request) {
        SessionHelper.sessionSetRole(PlayerRole.GUEST);
        SessionHelper.sessionSetID(-1);
        return new ResponseEntity<>( "You are successfully logged out", HttpStatus.OK);
    }

    /**
     * @return ResponseEntity with OK status and
     */
    @GetMapping("/me")
    @Role(role = { PlayerRole.USER, PlayerRole.ADMIN })
    public ResponseEntity<PlayerDataDTO> getPlayerData() {
        return new ResponseEntity<>(playerService.getPlayerData(), HttpStatus.OK);
    }

    @GetMapping("/my-id")
    @Role(role = { PlayerRole.USER, PlayerRole.ADMIN })
    public ResponseEntity<Long> getPlayerID() {
        return new ResponseEntity<>(SessionHelper.sessionGetID(), HttpStatus.OK);
    }

    @GetMapping("/logged")
    public ResponseEntity<Boolean> isLogged() {
        return new ResponseEntity<>(SessionHelper.sessionGetID() != -1 ? true : false, HttpStatus.OK);
    }

    @GetMapping("/confirm")
    public ResponseEntity<String> confirmToken(@RequestParam String token) {
        String message = "";
        try {
            message = registrationService.confirmToken(token);

        } catch (IllegalStateException e) {
            throw new EmailConfirmedException();
        }
        return new ResponseEntity<>(message, HttpStatus.OK);
    }

    @GetMapping("/history")
    @Role(role = {PlayerRole.USER, PlayerRole.ADMIN})
    public ResponseEntity<List<FinishedGame>> getHistory() {
        return new ResponseEntity<>(restGameService.getHistoryOfCurrentUser(), HttpStatus.OK);
    }

}
