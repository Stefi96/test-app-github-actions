package com.trade.rest.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(code = HttpStatus.FORBIDDEN, reason = "Email not confirmed!")
public class EmailNotConfirmedException extends RuntimeException {
}
