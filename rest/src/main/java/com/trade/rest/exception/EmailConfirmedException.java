package com.trade.rest.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(code = HttpStatus.FORBIDDEN, reason = "Email already confirmed!")
public class EmailConfirmedException extends RuntimeException {
}
