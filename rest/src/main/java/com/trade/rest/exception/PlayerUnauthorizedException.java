package com.trade.rest.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(value = HttpStatus.UNAUTHORIZED, reason = "You don't have autorization for current page!")
public class PlayerUnauthorizedException extends RuntimeException{

    public PlayerUnauthorizedException() {
        super();
    }

}
