--liquibase formatted sql
--changeset Koca:create-tables splitStatements:true endDelimiter:;

insert into player (id,username,email,password,player_role,locked,enabled)
            values (1,"user1","user1@gmail.com","$2a$10$ivFucRJJp.a0ayyPQ3.OnOBX/hbl3FCsYfoAn/4chCKFiBCRuok7C","USER",0,1);

insert into player (id,username,email,password,player_role,locked,enabled)
            values (2,"user2","user2@gmail.com","$2a$10$EwrjaetmeWjUkqXTwSpoEecJGENxzigvIQbGHgJKQNyUHach8/D3O","USER",0,1);

insert into player (id,username,email,password,player_role,locked,enabled)
            values (3,"user3","user3@gmail.com","$2a$10$j9q/xPY9NaxEgC79kiMWwuGUVkVi4ZLr/dIogHOEmxYSHmR27S1p2","USER",0,1);

insert into player (id,username,email,password,player_role,locked,enabled)
            values (4,"user4","user4@gmail.com","$2a$10$UJuRUMQfHVnINjJLPUwFC.85Pak9zsWxOs.xjctmKvgbn3L0q2/xS","USER",0,1);

