--liquibase formatted sql
--changeset Dusan:create-tables splitStatements:true endDelimiter:;

CREATE TABLE `finished_game` (
                                 `id` bigint(20) NOT NULL,
                                 `ended_at` datetime DEFAULT NULL,
                                 `player1id` bigint(20) NOT NULL,
                                 `player1score` int(11) NOT NULL,
                                 `player1username` varchar(255) DEFAULT NULL,
                                 `player2id` bigint(20) NOT NULL,
                                 `player2score` int(11) NOT NULL,
                                 `player2username` varchar(255) DEFAULT NULL,
                                 `started_at` datetime DEFAULT NULL,
                                 `winner_id` bigint(20) NOT NULL
);

ALTER TABLE `finished_game`
    ADD PRIMARY KEY (`id`);

ALTER TABLE `finished_game`
    MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=1;
COMMIT;