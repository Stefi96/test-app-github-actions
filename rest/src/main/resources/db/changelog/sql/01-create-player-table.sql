--liquibase formatted sql
--changeset Jovan:create-tables splitStatements:true endDelimiter:;

CREATE TABLE player (
    id INT PRIMARY KEY AUTO_INCREMENT,
    username VARCHAR(100),
    email VARCHAR(100),
    password VARCHAR(100),
    player_role VARCHAR(10),
    locked BIT,
    enabled BIT
);

CREATE TABLE confirmation_token (
    id INT PRIMARY KEY AUTO_INCREMENT,
    token VARCHAR(100),
    created_at DATETIME,
    expires_at DATETIME,
    confirmed_at DATETIME,
    player_id INT
)