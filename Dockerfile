FROM openjdk:11

COPY rest/target/rest-0.0.1-SNAPSHOT.jar app.jar

ENTRYPOINT ["java","-Dspring.profiles.active=prod", "-jar", "/app.jar"]