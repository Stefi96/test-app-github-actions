<h1 align="center">TnT Groupe Projec - Mancala Game</h1>

## Overview

This project is about a game called Mancala. It is a simple game where you play against other player.

The goal of game is to collect as many stones as you can in your mancala, which is a your storage. You achive that by having 6 pits each(player) that are filled with 4 stones each(pits) and on your turn>


## 🚀 Usage (local)

You will need [JDK 17](https://www.oracle.com/java/technologies/javase/jdk17-archive-downloads.html) installed

Also need [mvn](https://maven.apache.org/download.cgi) installed

Then you can use [XAMPP](https://www.apachefriends.org/download.html) control panel to start up data base

We used InetliJ as IDE of choies, from witch we startup Spring Boot Rest app and we use Visual Studio Code for our React UI

For React you will need [Node.js](https://nodejs.org/en/) installed so you can execute command :

```sh
npm install && npm start
```

If you encounter problem where you cannot do "npm install" do the same command with --force : 

```sh
npm install --force && npm start
```

After starting Spring Boot Rest app and React UI you can go to [localhost/3000](http://localhost:3000/) and play the game.

## Deploy

We have our application deployed on a VM so you can access it by going [HERE](http://20.13.8.193:3000/)

## Contibutors

This project exist thanks to all people who worked on it:
* GitHub:
  * [@kocaaa](https://github.com/kocaaa)
  * [@jbojovicct360](https://github.com/jbojovicct360)
  * [@martel7](https://github.com/martel7)
  * [@AleksaRistovicCT360](https://github.com/AleksaRistovicCT360)
  * [@4tradejimi](https://github.com/4tradejimi)

## 📝 License

<img style="width: 175px" src="https://www.comtrade360.com/wp-content/themes/comtrade360/images/comtrade-360.svg" />  Copyright © 2022 Comtrade 360 All Rights Reserved. [Terms of Use](https://www.comtra>

